function checkForNum(e) {
  e.value = e.value.replace(/[\D]/g, "")
}

$(function () {
  for (var c = "http://lipstick.51ulong.com", n = navigator.userAgent.toLowerCase(), inviteCode = function () {
      for (var e = 0 < location.search.length ?
        location.search.substring(1) : "", n = {}, a = e.length ? e.split("&") : [],
             t = null, o = null, c = null, i = a.length, d = 0; d < i; d++) t = a[d].split("="),
        o = decodeURIComponent(t[0]),
        c = decodeURIComponent(t[1]),
      o.length && (n[o] = c);
      return n
    }().inviteCode || "",
         shareCode = function () {
           for (var e = 0 < location.search.length ?
             location.search.substring(1) : "", n = {}, a = e.length ? e.split("&") : [],
                  t = null, o = null, c = null, i = a.length, d = 0; d < i; d++) t = a[d].split("="),
             o = decodeURIComponent(t[0]),
             c = decodeURIComponent(t[1]),
           o.length && (n[o] = c);
           return n
         }().shareCode || "",
         codeInput = $("#val-code-input"),
         d = $("#val-box .val-item"),
         s = /^[\d]+$/, l = 0,
         e = codeInput.val(),
         a = 0; a < e.length; a++) $(d[a]).text(e.substring(a, a + 1));


  function getSms(telePhone) {
    $.ajax({
      url: c + "/player/getSMSCode",
      type: "POST",
      dataType: "json",
      data: {
        phoneNum: telePhone
      },
      success: function (e) {
        true === e.ok ? (showTip("验证码已发送"), function () {
          var e = 60, n = $("#getCode");
          n.attr("disabled", !1);
          var a = setInterval(function () {
            1 <= e ? (e--, n.text(e + "s")) : (clearInterval(a), e = 60, n.text("重新发送"), n.attr("disabled", !1))
          }, 1e3)
        }()) : (showTip(e.data.msg !== "" ? e.data.msg : "验证码发送失败，请勿频繁发送，1分钟后再试")) // (showTip("验证码发送失败，请勿频繁发送，1分钟后再试"), geeTest.reset())
      }
    })
  }


  function showTip(e) {
    $(".common_pops").text(e).removeClass("none"), setTimeout(function () {
      $(".common_pops").addClass("none")
    }, 3e3)
  }

  codeInput.on("input propertychange change", function (e) {
    var n = codeInput.val(), a = n.length;
    if (l = a, n && s.test(n)) for (var t = 0; t < 6; t++) {
      var o = n.substr(t, 1);
      $(d[t]).text(o), o && $(d[t]).addClass("available")
    }
  }),



    $(this).on("keyup", function (e) {
      8 === e.keyCode && ($(d[l]).removeClass("available"), $(d[l]).text(""))
    }),

    $(document).on("click", "#getCode", function () {
      var phoneNum = $("#phoneNum").val();
      "" !== phoneNum ? /^\d{1,14}$/.test(phoneNum) ? getSms(phoneNum) : showTip("手机号码格式不正确") : showTip("手机号不能为空")
    }),




    $(document).on("click", "#register", function () {
      !function () {
        var phoneNum = $("#phoneNum").val(), smsCodeInput = $("#val-code-input").val(), countryCode = $("#countryCodes").val(),
          t = "+86" === countryCode ? phoneNum : countryCode.replace("+", "") + "_" + phoneNum;
        {
          if ("" === phoneNum) return showTip("手机号不能为空");
          if (!/^\d{1,14}$/.test(phoneNum)) return showTip("手机号码格式不正确")
        }
        if ("" === smsCodeInput) return showTip("验证码不能为空");
        $.ajax({
          url: c + "/player/register/invite",
          type: "POST",
          dataType: "json",
          data: {phoneNum, smsCode: smsCodeInput, inviteCode, shareCode, requestBy: 'html'},
          success: function (e) {
            true === e.ok ? (showTip("注册成功"), window.location.href = "https://potatso-lite.xcat.ml/") : (showTip(e.data.msg !== "" ? e.data.msg : "注册失败，账号已存在或验证码错误！"))// showTip("注册失败，账号已存在或验证码错误！")
          },
          fail: function () {
            showTip("申请失败，请稍后再试")
          }
        })
      }()
    });
  var u, m, v = [{name: "中国(China)", code: "+86"}, {name: "中国香港(Hong Kong)", code: "+852"}, {
    name: "中国澳门(Macao)",
    code: "+853"
  }, {name: "中国台湾(Taiwan)", code: "+886"}, {name: "美国(America)", code: "+1"}, {
    name: "加拿大(Canada)",
    code: "+1"
  }, {name: "日本(Japan)", code: "+81"}, {name: "韩国(South Korea)", code: "+82"}, {
    name: "越南(Vietnam)",
    code: "+84"
  }, {name: "柬埔寨(Cambodia)", code: "+855"}, {name: "泰国(Thailand)", code: "+66"}, {
    name: "新加坡(Singapore)",
    code: "+65"
  }, {name: "马来西亚(Malaysia)", code: "+60"}, {name: "印度尼西亚(Indonesia)", code: "+62"}, {
    name: "菲律宾(Philippines)",
    code: "+63"
  }, {name: "澳大利亚(Australia)", code: "+61"}];
  u = document.getElementById("codeWrapper"), m = "", v.forEach(function (e, n) {
    m += '<div class="code-line" data-code="' + e.code + '"> <div class="c333 c-name">' + e.name + "</div>", m += '<div class="cbdb c-code">' + e.code + "</div></div>"
  }), u.innerHTML = m, $(".country-select").click(function () {
    $(".fixed-bg").removeClass("none"), $(".countryCodeBox").removeClass("none").addClass("fadeInUp")
  }), $(".fixed-bg").click(function () {
    $(this).addClass("none"), $(".countryCodeBox").addClass("none").removeClass("fadeInUp")
  }), $("#codeWrapper").on("click", ".code-line", function () {
    var e = $(this).attr("data-code") || "+86";
    $(".fixed-bg").addClass("none"), $(".countryCodeBox").addClass("none").removeClass("fadeInUp"), $(".country-select #countryCodes").val(e)
  })
}), $(document).ready(function () {
  $(".mainBox").height($("body")[0].clientHeight)
});
