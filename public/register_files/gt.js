!function (t, e) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
        if (!t.document) throw new Error("Geetest requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function (c, t) {
    "use strict";
    if (void 0 === c) throw new Error("Geetest requires browser environment");
    var s = c.document, i = c.Math, f = s.getElementsByTagName("head")[0];

    function l(t) {
        this._obj = t
    }

    function e(t) {
        var n = this;
        new l(t)._each(function (t, e) {
            n[t] = e
        })
    }

    l.prototype = {
        _each: function (t) {
            var e = this._obj;
            for (var n in e) e.hasOwnProperty(n) && t(n, e[n]);
            return this
        }
    }, e.prototype = {
        api_server: "api.geetest.com",
        protocol: "http://",
        type_path: "/gettype.php",
        fallback_config: {
            slide: {
                static_servers: ["static.geetest.com", "dn-staticdown.qbox.me"],
                type: "slide",
                slide: "/static/js/geetest.0.0.0.js"
            },
            fullpage: {
                static_servers: ["static.geetest.com", "dn-staticdown.qbox.me"],
                type: "fullpage",
                fullpage: "/static/js/fullpage.0.0.0.js"
            }
        },
        _get_fallback_config: function () {
            var t = this;
            return u(t.type) ? t.fallback_config[t.type] : t.new_captcha ? t.fallback_config.fullpage : t.fallback_config.slide
        },
        _extend: function (t) {
            var n = this;
            new l(t)._each(function (t, e) {
                n[t] = e
            })
        }
    };
    var u = function (t) {
        return "string" == typeof t
    }, p = {}, d = {}, g = function (t, e, n, o) {
        e = e.replace(/^https?:\/\/|\/$/g, "");
        var r, a = (0 !== (r = (r = n).replace(/\/+/g, "/")).indexOf("/") && (r = "/" + r), r + function (t) {
            if (!t) return "";
            var n = "?";
            return new l(t)._each(function (t, e) {
                (u(e) || "number" == typeof e || "boolean" == typeof e) && (n = n + encodeURIComponent(t) + "=" + encodeURIComponent(e) + "&")
            }), "?" === n && (n = ""), n.replace(/&$/, "")
        }(o));
        return e && (a = t + e + a), a
    }, h = function (t, n, o, r, a) {
        var i = function (e) {
            !function (t, e) {
                var n = s.createElement("script");
                n.charset = "UTF-8", n.async = !0;
                var o = (n.onerror = function () {
                    e(!0)
                }, !1);
                n.onload = n.onreadystatechange = function () {
                    o || n.readyState && "loaded" !== n.readyState && "complete" !== n.readyState || (o = !0, setTimeout(function () {
                        e(!1)
                    }, 0))
                }, n.src = t, f.appendChild(n)
            }(g(t, n[e], o, r), function (t) {
                t ? e >= n.length - 1 ? a(!0) : i(e + 1) : a(!1)
            })
        };
        i(0)
    }, n = function (t, e, n, o) {
        if ("object" == typeof(r = n.getLib) && null !== r) return n._extend(n.getLib), void o(n);
        var r;
        if (n.offline) o(n._get_fallback_config()); else {
            var a = "geetest_" + (parseInt(1e4 * i.random()) + (new Date).valueOf());
            c[a] = function (t) {
                "success" === t.status ? o(t.data) : t.status ? o(n._get_fallback_config()) : o(t), c[a] = void 0;
                try {
                    delete c[a]
                } catch (t) {
                }
            }, h(n.protocol, t, e, {gt: n.gt, callback: a}, function (t) {
                t && o(n._get_fallback_config())
            })
        }
    }, _ = function (t, e) {
        var n = {networkError: "网络错误"};
        if ("function" != typeof e.onError) throw new Error(n[t]);
        e.onError(n[t])
    };
    c.Geetest && (d.slide = "loaded");
    var o = function (t, o) {
        var i = new e(t);
        t.https ? i.protocol = "https://" : t.protocol || (i.protocol = c.location.protocol + "//"), n([i.api_server || i.apiserver], i.type_path, i, function (t) {
            var a = t.type, e = function () {
                i._extend(t), o(new c.Geetest(i))
            };
            p[a] = p[a] || [];
            var n = d[a] || "init";
            "init" === n ? (d[a] = "loading", p[a].push(e), h(i.protocol, t.static_servers || t.domains, t[a] || t.path, null, function (t) {
                if (t) d[a] = "fail", _("networkError", i); else {
                    d[a] = "loaded";
                    for (var e = p[a], n = 0, o = e.length; n < o; n += 1) {
                        var r = e[n];
                        "function" == typeof r && r()
                    }
                    p[a] = []
                }
            })) : "loaded" === n ? e() : "fail" === n ? _("networkError", i) : "loading" === n && p[a].push(e)
        })
    };
    return c.initGeetest = o
});