module.exports = {
  apps: [
    {
      name: "gameserver",
      script: "dist/server.js",
      env: {
        COMMON_VARIABLE: "true"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }, {
      name: "crontab",
      script: "dist/schedules.js",
      env: {
        COMMON_VARIABLE: "true"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }, {
      name: "gm-tool",
      script: "dist/gm-tool/server.js",
      env: {
        COMMON_VARIABLE: "true"
      },
      env_production: {
        NODE_ENV: "production"
      }
    }
  ],
  deploy: {
    production: {
      user: "root",
      host: "106.14.180.14",
      ref: "origin/master",
      repo: "git@gitee.com:gyrocopter/lipstick-server.git",
      path: "/root/games/lipstick",
      "post-deploy": "yarn && yarn build && pm2 startOrRestart ecosystem.config.js --env production"
    }
  }
}
