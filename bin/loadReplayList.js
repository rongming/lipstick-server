#!/usr/bin/env node

const mongoose = require('mongoose')

const RoomRecord = require('../src/database/models/roomRecord').default
const GameRecord = require('../src/database/models/gameRecord').default
mongoose.connect("mongodb://localhost:27017/mahjong")
const cardToEmoji = {
  1: "🀇",
  2: "🀈",
  3: "🀉",
  4: "🀊",
  5: "🀋",
  6: "🀌",
  7: "🀈",
  8: "🀎",
  9: "🀏",

  11: "🀐",
  12: "🀑",
  13: "🀒",
  14: "🀓",
  15: "🀔",
  16: "🀕",
  17: "🀖",
  18: "🀗",
  19: "🀘",

  21: "🀙",
  22: "🀚",
  23: "🀛",
  24: "🀜",
  25: "🀝",
  26: "🀞",
  27: "🀟",
  28: "🀠",
  29: "🀡",
  31: "🀀",
  32: "🀁",
  33: "🀂",
  34: "🀃",
  35: "🀄️",
  36: "🀅",
  37: "🀆"
}


const roomNumber = process.argv[2]
console.log(roomNumber)

RoomRecord.findOne({roomNum: roomNumber})
  .sort({createAt: -1})
  .exec()
  .then(function (roomRecord) {

    return GameRecord
      .find({room: roomRecord.room})
      .exec()
  })
  .then((gameRecords) => {
    for (let gameRecord of gameRecords) {

      console.log('========>', gameRecord._id, gameRecord.time, '<========')
      console.log('财神', cardToEmoji[gameRecord.game.caiShen])
      const playersInfo = gameRecord.playersInfo

      for (let event of gameRecord.events) {

        const cardEmojis = event.info.cards.map((card) => cardToEmoji[card]).join(' ')
        const suits = event.info.suits.map(s => s.map(card => cardToEmoji[card]).join(' ')).join(' | ')
　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
        const name = (playersInfo[event.index].model.name + '　　　　　　　　').slice(0,4)

        console.log(name,
          `${(event.type + '     ').slice(0, 5)}`,
          cardToEmoji[event.info.card] || ' ', '|  ',
          cardEmojis, '  ', suits)
      }

    }
  })
  .catch(function (e) {
    console.error(e.stack)
  })
  .then(function () {
    process.exit()
  })
