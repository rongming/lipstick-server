#!/bin/env node

const mongoose = require('mongoose')
const GameRecord = require('../src/database/models/gameRecord').default
const cardToEmoji = {
  1: "🀇",
  2: "🀈",
  3: "🀉",
  4: "🀊",
  5: "🀋",
  6: "🀌",
  7: "🀈",
  8: "🀎",
  9: "🀏",

  11: "🀐",
  12: "🀑",
  13: "🀒",
  14: "🀓",
  15: "🀔",
  16: "🀕",
  17: "🀖",
  18: "🀗",
  19: "🀘",

  21: "🀙",
  22: "🀚",
  23: "🀛",
  24: "🀜",
  25: "🀝",
  26: "🀞",
  27: "🀟",
  28: "🀠",
  29: "🀡",
  31: "🀀",
  32: "🀁",
  33: "🀂",
  34: "🀃",
  35: "🀅",
  36: "🀄️",
  37: "🀆"
}


const gameRecordId = process.argv[2]


mongoose.connect("mongodb://localhost:27017/mahjong")
console.log(gameRecordId)
GameRecord.findOne({_id: gameRecordId}).exec()
  .then((gameRecord) => {

    for (let event of gameRecord.events) {

      const cardEmojis = event.info.cards.map((card) => cardToEmoji[card]).join(' ')
      const suits = event.info.suits.map(s=>s.map(card=>cardToEmoji[card]).join(' ')).join(' | ')

      console.log(`${event.index}`, 
                  `${(event.type+ '     ').slice(0,5)}`, 
                  cardToEmoji[event.info.card]|| ' ', '|  ',
                  cardEmojis,'  ',suits)
    }

  })
  .catch((error) => {
    console.error(error.stack)
  })
  .then(() => {
    process.exit()
  })


