import * as config from 'config'
import * as moment from 'moment'
import * as mongoose from 'mongoose'
import ms = require("ms");
import {PaymentOrderModel} from "../database/models/paymentOrder";
import {RechargeOrderModel} from "../database/models/rechargeOrder";
import {RechargeSummaryModel} from '../database/models/rechargeSummary';

async function aggregateRechargeRecord(day: Date = new Date()) {
  const start = moment(day).startOf('day').toDate()
  const end = moment(day).endOf('day').toDate()

  const wechat = {recharges: 0, sum: 0, type: `wechatPay`}
  const ali = {recharges: 0, sum: 0, type: `aliPay`}
  const mySum = await RechargeOrderModel.find({createAt: {$gte: start, $lt: end}, state: 'paid'}).lean()
  for (const it of mySum) {
    const po = await PaymentOrderModel.findOne({order: it._id.toString()}).lean()
    if (!po)
      continue
    if (po.paymentType === `wechatPay`) {
      wechat.recharges++
      wechat.sum += it.RMBCost
      continue
    }
    if (po.paymentType === `aliPay`) {
      ali.recharges++
      ali.sum += it.RMBCost
      continue
    }
  }

  const summary = [wechat, ali]
  for (const s of summary) {
    if (s.recharges === 0 && s.sum === 0)
      continue
    await RechargeSummaryModel.updateOne({
      day: start,
      type: s.type
    }, {
      $set: {
        recharges: s.recharges,
        sum: s.sum
      }
    }, {upsert: true})
  }
}

export default aggregateRechargeRecord

if (!module.parent) {

  mongoose.connect(config.get('database.url'))
  console.log(config.get('database.url'))

  const dateString = process.argv[2]

  console.log(`querying for date`, moment(new Date(dateString)).startOf('day'));

  if (false) {
    // @ts-ignore
    aggregateRechargeRecord(new Date(dateString))
      .catch((error) => {
        console.error('Got', error)
      })
      .then(() => {
        process.exit()
      })
  } else {
    // @ts-ignore
    for (let i = 0; i < 100; i++) {
      aggregateRechargeRecord(new Date(Date.now() - ms(i.toString() + `days`)))
        .catch((error) => {
          console.error('Got', error)
        })
        .then(() => {
          // process.exit()
        })
    }
  }
}
