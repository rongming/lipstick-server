import * as fs from "fs";

export type Quiz = { question: string, answer: string, wrongAnswers: string[] }

function clearQuestion(q: string): string {
  return q.trim().replace(/^\d+[\.、]*/, '').trim()
}

function clearAnswer(text: string): string {
  return text.trim().replace(/^[、.]/, '').trim()
}

export function parseLine(line): Quiz {
  const parts = line.split(/[ABCDEFGＡＢＣＤ]/g)

  if (parts.length <= 2) {
    console.error(line)
    throw Error("wrong data")
  }

  return {
    question: clearQuestion(parts[0]),
    answer: clearAnswer(parts[1]),
    wrongAnswers: parts.slice(2).map(clearAnswer)
  }
}

export function parseText(text: string): Quiz[] {
  const quizzes = text.split('\n')
    .filter(l => l)
    .map(parseLine)
  return quizzes
}

if (!module.parent) {

  const file = process.argv[2]

  const text = fs.readFileSync(file, 'utf-8')

  const quizzes = parseText(text)

  console.log(JSON.stringify(quizzes, null, ' '))

}
