import * as fs from "fs";
import * as config from 'config'
import * as mongoose from 'mongoose'
import {QuizModel} from "../database/models/quiz";

const jsonFile = process.argv[2]


async function main() {

  console.error(config.get('database.url'));
  await mongoose.connect(config.get('database.url'))
  const quizzes = JSON.parse(fs.readFileSync(jsonFile, 'utf-8'))

  for (const q of quizzes) {
    try {
      await QuizModel.create(q)
    } catch (e) {
      console.error(e.message)
      console.error(q)
    }
  }

  console.error('done')
}

main()
  .catch((error) => {
    console.error(error)
  })
  .then(() => {
    return mongoose.disconnect()
  })

