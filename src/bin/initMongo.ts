import * as config from 'config'
import * as mongoose from 'mongoose'
import {mkCdKeys} from "../api/utils";
import {CdkeyModel} from "../database/models/cdkey";
import {LipstickSeriesModel} from "../database/models/lipstickSeries";
import {PublicMailModel} from "../database/models/mail";
import {PlayerModel} from "../database/models/player";
import {ProductModel} from "../database/models/Product";
import {PressGroupModel, ProductGroupModel} from "../database/models/ProductGroup";
import {RechargeModel} from "../database/models/recharge";
import {testPressGroup} from "../test/initData/data";
import {
  initMongoCdkey,
  initMongoLipstickSeries,
  initMongoLittleGift, initMongoPointProduct, initMongoPressGroup,
  initMongoProduct, initMongoPublicMail,
  initMongoRecharge
} from "../test/initData/initMongoData";
import ms = require("ms");

async function main() {
  if (process.env.NODE_ENV !== 'initMongo')
    return

  console.error(config.get('database.url'));
  await mongoose.connect(config.get('database.url'))

  await setupInitMongoEnv()

  console.error('done')
}

const randomString = () => Math.random().toFixed(10).toString()
const randomProduct = (p) => ({
  ...p,
  model: `chili_${randomString()}`,
  stock: 600
})
const randomLittleGift = () => ({
  ...initMongoLittleGift,
  model: `chili_${randomString()}`,
  stock: 600
})

async function setupInitMongoEnv() {
  /*const product = await ProductModel.create(initMongoProduct)

  const product1 = await ProductModel.create(randomProduct(initMongoProduct))
  const product2 = await ProductModel.create(randomProduct(initMongoProduct))
  const product3 = await ProductModel.create(randomProduct(initMongoProduct))
  const product4 = await ProductModel.create(randomProduct(initMongoProduct))*/

  /*const pointProduct = await ProductModel.create(randomProduct(initMongoPointProduct))
  const pointProduct1 = await ProductModel.create(randomProduct(initMongoPointProduct))*/

  /*const gift = await ProductModel.create(randomLittleGift())
  const littleGift1 = await ProductModel.create(randomLittleGift())
  const littleGift2 = await ProductModel.create(randomLittleGift())
  const littleGift3 = await ProductModel.create(randomLittleGift())
  const littleGift4 = await ProductModel.create(randomLittleGift())

  const productGroup = await ProductGroupModel.create({
    name: '201900428-10:28',
    description: "201900428-10:28",
    priceInCoin: 100,
    level1: [product1, gift, littleGift1, littleGift2],
    level2: [product2, littleGift3, littleGift4],
    level3: [product3, littleGift4],
    level4: [product4],
    state: 'on',
    category: `eggDraw`,
    priceInGem: 100
  })

  const productGroup2 = await ProductGroupModel.create({
    name: '201900428-10:28 2222',
    description: "201900428-10:28 2222",
    priceInCoin: 100,
    priceInGem: 100,
    level1: [await ProductModel.create(randomProduct(initMongoProduct)),
      await ProductModel.create(randomProduct(initMongoProduct)),
      await ProductModel.create(randomProduct(initMongoProduct)), await ProductModel.create(randomProduct(initMongoProduct))],
    level2: [await ProductModel.create(randomProduct(initMongoProduct)),
      await ProductModel.create(randomProduct(initMongoProduct)), await ProductModel.create(randomProduct(initMongoProduct))],
    level3: [await ProductModel.create(randomProduct(initMongoProduct)),
      await ProductModel.create(randomProduct(initMongoProduct))],
    level4: [await ProductModel.create(randomProduct(initMongoProduct))],
    state: 'on',
    category: `luckyBox`
  })

  const lipstickSeries = await LipstickSeriesModel.create({
    ...initMongoLipstickSeries,
    name: '口红机1 201900428-10:28',
    products: [product, product1, product2],
    category: 'lipstick'
  })

  const lipstickSeries2 = await LipstickSeriesModel.create({
    ...initMongoLipstickSeries,
    name: '口红机2 201900428-10:28',
    products: [product3, product4],
    category: 'lipstick'
  })

  const lipstickSeries3 = await LipstickSeriesModel.create({
    ...initMongoLipstickSeries,
    products: [await ProductModel.create(randomProduct(initMongoProduct)), await ProductModel.create(randomProduct(initMongoProduct))],
    category: 'pressAccurate',
    name: '按的准 201900428-10:28'
  })
  const lipstickSeries4 = await LipstickSeriesModel.create({
    ...initMongoLipstickSeries,
    products: [await ProductModel.create(randomProduct(initMongoProduct)), await ProductModel.create(randomProduct(initMongoProduct))],
    category: 'pressAccurate',
    name: '按的准 201900428-10:28'
  })

  const pressGroup = await PressGroupModel.create({
    ...initMongoPressGroup,
    level1: [lipstickSeries3, lipstickSeries4],
    level2: [lipstickSeries3, lipstickSeries4],
    level3: [lipstickSeries3],
    name: '按的准 1 201900428-10:28',
  })

  const pressGroup2 = await PressGroupModel.create({
    ...initMongoPressGroup,
    level1: [lipstickSeries3, lipstickSeries4],
    level2: [lipstickSeries3, lipstickSeries4],
    level3: [lipstickSeries3],
    name: '按的准2 201900428-10:28'
  })

  for (let j = 0; j < 6; j++) {
    await RechargeModel.create({
      ...initMongoRecharge,
      coin: 100 * Math.pow(2, j),
      freeCoin: 10 * Math.pow(2, j),
      gem: 0,
    })
  }

  for (let j = 0; j < 6; j++) {
    await RechargeModel.create({
      ...initMongoRecharge,
      coin: 0,
      freeCoin: 0,
      gem: 100 * Math.pow(2, j),
    })
  }*/
  const reward = {
    coin: 100,
    freeCoin: 10,
    gem: 70,
    point: 20
  }
  const cdKeyOneToAll = await CdkeyModel.create({
    name: 'ulong活动兑换码',
    keys: ['ulong'],
    type: 'OneToAll',
    ...initMongoCdkey
  })

  /*const p = await PlayerModel.find({name: /手机用户/})
  const ary = []
  for (const it of p)
    ary.push(it.id)

  const cdKeyOneToOne = await CdkeyModel.create({
    name: '老手机用户专属兑换码',
    keys: ['hello'],
    type: 'OneToSome',
    toPlayers: ary,
    ...initMongoCdkey
  })*/

  const cdKeyAllToAll = await CdkeyModel.create({
    name: '六一儿童节 2022年前限量兑换码',
    keys: mkCdKeys(100),
    type: 'AllToAll',
    ...initMongoCdkey
  })

  await PublicMailModel.create({
    ...initMongoPublicMail,
    content: `活动一，2022年前，每人都可以输入ulong兑换活动一的钱；另外，活动二截止2022年，下列兑换码先到先得：${cdKeyAllToAll.keys}`
  })
}

main()
  .catch((error) => {
    console.error(error)
  })
  .then(() => {
    return mongoose.disconnect()
  })
