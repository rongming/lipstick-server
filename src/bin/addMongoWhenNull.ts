import * as config from 'config'
import * as crypto from 'crypto'
import * as mongoose from 'mongoose'
import {Types} from "mongoose";
import {isNullOrUndefined} from "util";
import {getMD5, getSkuId} from "../api/utils";
import {CdkeyModel} from "../database/models/cdkey";
import {ConsumeRecordModel} from "../database/models/consumeRecord";
import {DeliveryModel} from "../database/models/delivery";
import {LipstickSeriesModel} from "../database/models/lipstickSeries";
import {PlayerModel} from "../database/models/player";
import {Product, ProductModel} from "../database/models/Product";
import {PressGroupModel, ProductGroupModel} from "../database/models/ProductGroup";
import {RechargeOrderModel} from "../database/models/rechargeOrder";
import {
  initMongoProduct,
} from "../test/initData/initMongoData";

async function main() {
  console.error("mongodb://localhost:27021/luckybox");
  await mongoose.connect("mongodb://localhost:27021/luckybox")

  await addData()
  console.error('done')
}

async function addData() {
  const ps = await LipstickSeriesModel.find().lean()
  for (const it of ps) {
    if (!it.hotNum)
      await LipstickSeriesModel.findByIdAndUpdate(it._id, {$set: {hotNum: 0}})
  }
}

main()
  .catch((error) => {
    console.error(error)
  })
  .then(() => {
    return mongoose.disconnect()
  })
