import * as redis from 'redis'
import {shuffle} from 'lodash'
import config from '../utils/config'

const start = 111111
const end = 999999

let roomIds = new Array()

for (let id = start; id <= end; id++) {
  roomIds.push(id)
}

roomIds = shuffle(roomIds)


const client = redis.createClient({host: config.get<string>('redis.host')})


client.del('roomIds', function () {
  client.lpush('roomIds', roomIds, function () {

    console.log(`${__filename}:18 `, roomIds.length, 'put in redis');

    client.quit()
  })
})
