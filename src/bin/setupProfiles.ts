import {readFileSync} from 'fs'
import {shuffle} from 'lodash'
import {join} from 'path'
import createRedisClient from "../utils/redis";

async function main() {
  const redis: any = await createRedisClient()
  let profiles = readFileSync(join(__dirname, '..', '..', 'names.json'), 'utf-8').trim().split('\n')
    .filter(s => s.length > 150)
  profiles = shuffle(profiles)

  console.log(`${__filename}:12 main`, profiles);

  await redis.delAsync('profiles')
  await redis.lpushAsync('profiles', profiles)

  console.log(`${profiles.length} profiles have been added`);
}

if (!module.parent) {
  main()
}
