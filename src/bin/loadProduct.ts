import * as mongoose from "mongoose";
import * as path from "path";
import * as config from 'config'
import {ProductModel} from "../database/models/Product";


const jsonFile = process.argv[2]


const json = require(path.join(process.cwd(), jsonFile))


const items = json.results.item

async function main() {

  await mongoose.connect(config.get('database.url'))


  const products = items.map(({id, name, title, color, pic_url, sale_price, game_price}) => {

    return {
      skuId: `${id}`,
      brand: name,
      name: title,
      marketPrice: sale_price,
      priceInCoin: game_price,
      coverUrl: pic_url,
      category: 'lipstick',
      description: color,
      tag: name,
      state: 'on',
    }
  })

  console.log(products);

  await ProductModel.create(products)

}

main()
  .catch(console.error)
  .then(
    () => {
      return mongoose.disconnect()
    }
  )
