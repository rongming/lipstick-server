import * as config from 'config'
import {readFileSync} from 'fs'
import {sample, shuffle} from 'lodash'
import * as mongoose from 'mongoose'
import {join} from 'path'
import {putDataInBroadcastInfo} from "../api/utils";
import {LipstickSeriesModel} from "../database/models/lipstickSeries";
import {PressGroupModel, ProductGroupModel} from "../database/models/ProductGroup";
import createRedisClient from "../utils/redis";

async function broadcastGame() {
  const redis: any = await createRedisClient()
  const nums = []
  for (let i = 0; i < 10; i++) {
    nums.push(i)
  }
  const stop = Math.random() * 5 + 5

  let pros = []
  for (const i of nums) {
    if (i > stop)
      break
    pros.push(await redis.rpopAsync(`profiles`))
  }
  pros = shuffle(pros)
  await redis.lpushAsync('profiles', pros)
  const player = {name: `「${JSON.parse(pros[0]).name.split(',')[0]}」` || '手机用户 6888'}
  const type = sample([`口红机` , `扭蛋机`, `幸运盒子`, `按得准`])
  const prize = {productName: `魅可小辣椒`, info: `获得了`}
  switch (type) {
    case `口红机`:
      const lips = await LipstickSeriesModel.find({onStock: true}).lean()
      if (!lips.length)
        break
      const lip = await LipstickSeriesModel.findById(sample(lips)._id).populate(`products`).lean()
      if (!lip)
        break
      prize.productName = sample(lip.products).name
      break
    case `扭蛋机`:
      const eggs = await ProductGroupModel.find({
        state: {$in: ['on', 'preview', 'limited']},
        category: 'eggDraw'
      }).lean()
      if (!eggs.length)
        break
      const egg = await ProductGroupModel.findById(sample(eggs)._id)
        .populate(`level1`)
        .populate(`level2`)
        .populate(`level3`)
        .populate(`level4`)
        .lean()
      if (!egg)
        break
      const egs = [].concat(egg.level1).concat(egg.level2).concat(egg.level3).concat(egg.level4)
      if (!egs.length)
        break
      prize.productName = sample(egs).name
      break
    case `幸运盒子`:
      const luckys = await ProductGroupModel.find({
        state: {$in: ['on', 'preview', 'limited']},
        category: 'luckyBox'
      }).lean()
      if (!luckys.length)
        break
      const lucky = await ProductGroupModel.findById(sample(luckys)._id)
        .populate(`level1`)
        .populate(`level2`)
        .populate(`level3`)
        .populate(`level4`)
        .lean()
      if (!lucky)
        break
      const lucks = [].concat(lucky.level1).concat(lucky.level2).concat(lucky.level3).concat(lucky.level4)
      if (!lucks.length)
        break
      prize.productName = sample(lucks).name
      break
    case `按得准`:
      const presses = await PressGroupModel.find({state: {$in: ['on', 'preview', 'limited']}}).lean()
      if (!presses.length)
        break
      const press = await PressGroupModel.findById(sample(presses)._id)
        .populate(`level1`)
        .populate(`level2`)
        .populate(`level3`)
        .lean()
      if (!press)
        break
      const n = sample([2, 3])
      if (!press[`level${n}`].length)
        break
      const pre = await LipstickSeriesModel.findById(sample(press[`level${n}`])._id).populate(`products`).lean()
      prize.productName = sample(pre.products).name
      prize.info = `中了${4 - n}等奖` + prize.info
      break
  }
  const broadcastInfo = {
    name: player.name,
    from: type,
    info: prize.info + prize.productName,
    createAt: new Date(Date.now())
  }
  await putDataInBroadcastInfo(broadcastInfo)
}

export default broadcastGame

async function main() {
  const redis: any = await createRedisClient()
  await mongoose.connect(config.get('database.url'))
console.log(config.get('database.url'))
  const profiles = readFileSync(join(__dirname, '..', '..', 'names.json'), 'utf-8').trim().split('\n')
    .filter(s => s.length > 150)

  // console.log(`${__filename}:13 main`, profiles);

  await redis.delAsync('profiles')
  await redis.lpushAsync('profiles', profiles)

  console.log(`${profiles.length} profiles add`);

  await broadcastGame()
    .catch(reason => console.log(reason))
}

if (!module.parent) {
  main()
}
