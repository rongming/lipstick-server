import {SubPhaseName, TableSubPhaseBase} from "./base";


export class GameOverPhase extends TableSubPhaseBase {
  name(): SubPhaseName {
    return "GameOverPhase";
  }

  resume() {
    if (this.table.state === 'gameOver') {
      return
    }
    this.onEntry()
  }

  onEntry() {
    this.table.gameOver()
  }

  onPlayerSubmitAnswer() {
    this.table.gameOver()
  }

  onLeave() {
  }
}
