import {Serializable, serializeHelp} from "../../serializeDecorator";
import NormalTable, {ITableHandler} from "../normalTable";
import PlayerState from "../playerAgent";

export type SubPhaseName =
  'mingPaiPhase'
  | 'QuizPhase'
  | 'preStartGamePhase'
  | 'dumpGuardPhase'
  | 'DelayPhase'
  | 'GameOverPhase'

export abstract class TableSubPhaseBase implements ITableHandler, Serializable {

  constructor(readonly table: NormalTable) {
  }

  abstract onEntry()

  abstract onLeave()

  abstract name(): SubPhaseName

  abstract resume(json: any)

  onPlayerBuJiao(player: PlayerState) {
    player.sendMessage('game/buJiaoReply', {ok: false, info: '不是叫分阶段.'})
  }

  onPlayerJiaoFen(player: PlayerState, jiaoFenInfo) {
    player.sendMessage('game/jiaoFenReply', {ok: false, info: '不是叫分阶段.'})
  }

  onPlayerQiangDiZhu(player: PlayerState) {
    player.sendMessage('game/qiangDiZhuReply', {ok: false, info: '不是抢地主阶段'})
  }

  onPlayerBuQiang(player: PlayerState) {
    player.sendMessage('game/buQiangReply', {ok: false, info: '不是抢地主阶段'})
  }

  onPlayerDa(player: PlayerState) {
    player.sendMessage('game/daReply', {ok: false, info: '不是打牌阶段'})
  }

  onPlayerGuo(player: PlayerState) {
    player.sendMessage('game/guoReply', {ok: false, info: '不是过牌阶段'})
  }

  onPlayerMingPai(player: PlayerState) {
    player.sendMessage('game/mingPaiReply', {ok: false, info: '不是名牌阶段'})
  }

  toJSON() {
    return serializeHelp(this)
  }

  onPlayerRequestHint(player: PlayerState) {
    player.sendMessage('game/requestHintReply', {ok: false, info: '暂时不'})
  }

  onPlayerSubmitAnswer(player: PlayerState, answer) {
    player.sendMessage('game/submitScoreReply', {ok: false, info: '现在还不能提交答案'})
  }
}


export class DumpPhase extends TableSubPhaseBase {
  name(): SubPhaseName {
    return 'dumpGuardPhase';
  }

  onEntry() {
  }

  onLeave() {
  }

  resume() {

  }
}


export function timeLeft(startAt: number, wait: number, now: number = Date.now()) {
  return startAt + wait - now
}
