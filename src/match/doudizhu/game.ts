import Rule from './Rule'
import NormalTable from './normalTable'
import {autoSerialize, Serializable, serialize, serializeHelp} from "../serializeDecorator";

export default class Game implements Serializable {
  @serialize
  rule: Rule

  @autoSerialize
  juShu: number

  @autoSerialize
  juIndex: number

  constructor(ruleObj) {
    this.rule = new Rule(ruleObj)
    this.juShu = ruleObj.juShu
    this.juIndex = 0
    this.reset()
  }

  startGame(room) {
    this.juShu--
    this.juIndex++
    return this.createTable(room)
  }

  toJSON() {
    return serializeHelp(this)
  }

  createTable(room) {
    return new NormalTable(room, this.rule, this.juShu)
  }

  reset() {
    this.juShu = this.rule.juShu
    this.juIndex = 0
  }

  isAllOver(): boolean {
    return this.juShu === 0
  }
}

