/**
 * Created by Color on 2016/9/2.
 */
import Enums from './enums';
import Card, {CardType} from "./card";

const fallBackQuizData = [
  {
    "question": "你所有的头发加起来可以承受多重",
    "answer": "12吨",
    "wrongAnswers": ["12斤", "12公斤", "12克"]
  },
  {
    "question": "中子星的一个微尘就有",
    "answer": "10亿吨",
    "wrongAnswers": ["1亿吨", "100亿吨", "1000亿吨"]
  },
  {
    "question": "人体里每立方英寸骨头的强度是混凝土的几倍",
    "answer": "4倍",
    "wrongAnswers": ["2倍", "3倍", "5倍"
    ]
  },
  {
    "question": "母虎与雄狮交配生产下的后代叫",
    "answer": "狮虎兽",
    "wrongAnswers": ["虎狮兽", "幼师", "幼虎"]
  },
  {
    "question": "哪种动物的精子长度第一",
    "answer": "果蝇",
    "wrongAnswers": ["老鼠", "人类", "大象"]
  }]


class Rule {

  ro: any

  constructor(ruleObj: any) {
    this.ro = ruleObj;
  }

  get wanFa(): string {
    return this.ro.wanFa
  }

  get clubPersonalRoom() {
    return this.ro.clubPersonalRoom
  }


  get isLuoSong(): boolean {
    return this.wanFa === 'luoSong'
  }

  getOriginData() {
    return this.ro
  }

  get share(): boolean {
    return !!this.ro.share
  }

  get ruleType() {
    return this.ro.ruleType || Enums.ruleType.lobby4Player;
  }

  get juShu() {
    return this.ro.juShu || 1
  }

  get playerCount() {
    return this.ro.playerCount || 4
  }

  get autoCommit() {
    return this.ro.autoCommit
  }

  get maPaiArray(): Card[] {
    const maPaiArr = this.ro.maPaiArray || []
    return maPaiArr.map(maPai => new Card(CardType.Heart, maPai))
  }

  get useJoker(): boolean {
    return this.ro.useJoker
  }

  get shaoJi() {
    return this.ro.shaoJi
  }

  get allBombScore() {
    return this.ro.quanJiang
  }

  get daDi() {
    return this.ro.daDi
  }

  get biDa() {
    return this.ro.biDa
  }

  get biGen() {
    return this.ro.biGen
  }

  get maxBase() {
    return Number(this.ro.maxBase) || 64
  }

  get quizzes() {
    return this.ro.quizzes || fallBackQuizData
  }

  get isPublic() {
    return this.ro.isPublic
  }
}


export default Rule;
