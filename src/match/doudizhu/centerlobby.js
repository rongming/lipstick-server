/**
 * Created by user on 2016-07-05.
 */

import {QuizModel} from "../../database/models/quiz";
import Room from './room';
import {LobbyFactory} from '../lobbyFactory'

const Lobby = LobbyFactory({
  gameName: 'doudizhu',
  roomFactory: async function (id, rule) {
    const room = new Room(rule);
    room._id = id;
    return room
  },
  // fixme: Room 被循环引用, 暂时采用函数调用来延迟 ref roomFee
  roomFee: () => 0,
  async ruleNormalize(rule) {
    const quizzes = await QuizModel.aggregate({$sample: {size: 5}})
    return {...rule, quizzes}
  }
})

export default Lobby;
