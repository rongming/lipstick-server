import {AsyncRedisClient} from "../utils/redis"
import {GameTypes} from "./gameTypes"


export class RoomRegister {

  redis: AsyncRedisClient

  constructor(redis: any) {
    this.redis = redis
  }

  async putPlayerInGameRoom(player: string, game: GameTypes, roomNumber: string) {
    return this.redis.hsetAsync(`u:${player}`, game, roomNumber)
  }

  async removePlayerFromGameRoom(player: string, game: GameTypes) {
    return this.redis.hdelAsync(`u:${player}`, game)
  }

  async allRoomsForPlayer(player: string) {
    return this.redis.hgetallAsync(`u:${player}`)
  }

  async roomNumber(player: string, game: GameTypes): Promise<number | null> {
    const roomNumber = await this.redis.hgetAsync(`u:${player}`, game)

    if (roomNumber) {
      const exists = await this.redis.getAsync(`room:${roomNumber}`)
      if (exists)
        return Number(roomNumber)
    }
    return null
  }


  async logRoomExists(roomNumber) {

  }

  async removeRoom(roomNumber) {

  }
}
