import {expect, requestToApiServer, setupTestEnv, tearDownTestEnv, XMLbuilder} from "./helper";
import {
  putDataInBroadcastInfo,
  getBroadcastInfo,
  recordReadBroadcastPlayer,
  getReadBroadcastPlayer,
  delRedis
} from "../../api/utils";

describe('滚动播放', () => {
  const data1 = {
    name: '姓名1',
    from: '按的准',
    info: `获得 等奖，奖品为：`,
    createAt: new Date(Date.now())
  }
  const data2 = {
    name: '姓名2',
    from: '按的准',
    info: `获得 等奖，奖品为：`,
    createAt: new Date(Date.now())
  }
  const data3 = {
    name: '姓名3',
    from: '按的准',
    info: `获得 等奖，奖品为：`,
    createAt: new Date(Date.now())
  }
  const testRedisName1 = 'testBroadcastInfo'
  const playerId = 'testPlayer_id'

  beforeEach(async () => {
    delRedis(testRedisName1)
    delRedis(playerId)
  })

  it('读取broadcast数据', async () => {
    await putDataInBroadcastInfo(data1, testRedisName1)
    const d = await getBroadcastInfo(testRedisName1)
    await recordReadBroadcastPlayer(playerId, d.length)

    const alreadyReadNum = await getReadBroadcastPlayer(playerId)

    expect(alreadyReadNum).to.equal('1')
  })

  it('重复读取broadcast数据', async () => {
    await putDataInBroadcastInfo(data1, testRedisName1)
    const d1 = await getBroadcastInfo(testRedisName1)
    await recordReadBroadcastPlayer(playerId, d1.length)
    const alreadyReadNum1 = await getReadBroadcastPlayer(playerId)

    await putDataInBroadcastInfo(data2, testRedisName1)
    const d2 = await getBroadcastInfo(testRedisName1)
    await recordReadBroadcastPlayer(playerId, d2.length)
    const alreadyReadNum2 = await getReadBroadcastPlayer(playerId)

    expect(d1.length).to.equal(1)
    expect(d2.length).to.equal(2)
    expect(alreadyReadNum1).to.equal('1')
    expect(alreadyReadNum2).to.equal('2')
  })

  it('玩家未读', async () => {
    const alreadyReadNum = await getReadBroadcastPlayer(playerId)
    expect(alreadyReadNum).to.equal('0')
  })
})
