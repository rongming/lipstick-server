import {requestToApiServer, setupTestEnv, tearDownTestEnv, expect} from "./helper";

describe('测试充值接口(仅测试环境可用)', () => {

  let jwt
  let player

  before(async () => {
    const env = await setupTestEnv()

    jwt = env.jwt
    player = env.player
  })

  after(() => {
    return tearDownTestEnv();
  })


  it('给玩家加资源', async () => {


      const {body} = await requestToApiServer()
        .post('/dev/resource')
        .set({'x-jwt': jwt})
        .send({coin: 100, gem: 99})
        .expect(200)

      expect(body).to.have.properties({ok: true})

    }
  )

})
