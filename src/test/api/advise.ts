import * as request from 'supertest'
import AdvisementModel from "../../database/models/advisement";
import app from "../../serverApp";
import {testPublicMail} from "../initData/data";
import {expect, setupTestEnv, tearDownTestEnv, TestEnv} from "./helper";

describe('建议/反馈', () => {

  let env: TestEnv = null
  let player = null
  before(async () => {
    env = await setupTestEnv()

    player = env.player
  })

  after(async () => {
    await tearDownTestEnv()
  })
  beforeEach(async () => {
  })

  afterEach(async () => {
    // await MailModel.deleteMany({})
  })

  it('提交建议/反馈', async () => {
    await request(app)
      .post(`/advise`)
      .send({content:'内容', phoneNum: '112', qqNum: '889944'})
      .set({'x-jwt': env.jwt})
      .then(res => {
        expect(res.body.ok).to.be.true
        expect(res.body.data.info).to.equal('反馈提交成功！')
      })


  })

})
