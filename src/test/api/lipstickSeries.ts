import * as chai from 'chai'
import * as chaiProperties from 'chai-properties'
import * as request from 'supertest'
import {InstanceType} from 'typegoose'
import {LipstickSeries} from "../../database/models/lipstickSeries";
import {Product} from "../../database/models/Product";
import app from '../../serverApp'
import {setupTestEnv, tearDownTestEnv} from "./helper";

chai.use(chaiProperties)
const {expect} = chai

describe('口红机系列API', () => {

  let productModel: InstanceType<Product> = null
  let gift: InstanceType<Product> = null
  let jwt: string = null
  let lipstickSeries: InstanceType<LipstickSeries> = null

  before(async () => {

    const env = await setupTestEnv()
    productModel = env.product
    gift = env.gift
    jwt = env.jwt
    lipstickSeries = env.lipstickSeries
  })

  after(async () => {
    return tearDownTestEnv()
  })

  it('获取所有系列', () => {

    return request(app)
      .get('/lipstickSeries')
      .set({'x-jwt': jwt})
      .then(({body}) => {
        expect(body).to.have.properties({
          ok: true, data: {
          }
        })
      })
  })

  it('获取单个系列', () => {

    return request(app)
      .get(`/lipstickSeries/${lipstickSeries.id}`)
      .set({'x-jwt': jwt})
      .then(({body}) => {

        expect(body).to.have.properties({
          ok: true, data: {
            lipstickSeries: {_id: lipstickSeries.id}
          }
        })
      })
  })
})
