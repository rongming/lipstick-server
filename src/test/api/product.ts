import * as chai from 'chai'
import * as chaiProperties from 'chai-properties'
import {InstanceType} from 'typegoose'
import {GamePrizeModel} from "../../database/models/gamePrize";
import {Player, PlayerModel} from "../../database/models/player";
import {Product} from "../../database/models/Product";
import {ProductGroup} from "../../database/models/ProductGroup";
import {requestToApiServer, setupTestEnv, tearDownTestEnv, TestEnv} from "./helper";

chai.use(chaiProperties)
const {expect} = chai

describe('商品/商品组组API 获取', () => {

  let productModel: InstanceType<Product> = null
  let gift: InstanceType<Product> = null
  let jwt: string = null
  let pointP: InstanceType<Product> = null
  let player: InstanceType<Player> = null
  let productGroup: InstanceType<ProductGroup> = null

  before(async () => {

    const env = await setupTestEnv()
    productModel = env.product
    gift = env.gift
    jwt = env.jwt
    pointP = env.pointProduct
    player = env.player
    productGroup = env.productGroup
  })

  after(async () => {
    return tearDownTestEnv()
  })

  it('获取单品', async () => {

    return await requestToApiServer()
      .get(`/product/${productModel.id}`)
      .then(({body}) => {

        expect(body).to.have.properties({
          ok: true, data: {
            product: {_id: productModel.id}
          }
        })
      })
  })

  it('商品组获取', async () => {

    return requestToApiServer()
      .get('/product/group/eggDraw')
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({ok: true})
        expect(body.data.groups[0].level1).to.have.lengthOf(4)
        expect(body.data.groups[0].level2).to.have.lengthOf(3)
        expect(body.data.groups[0].level3).to.have.lengthOf(2)
        expect(body.data.groups[0].level4).to.have.lengthOf(1)
        expect(body.data.groups[0]).to.have.property('highestPrice')
      })
  })

  it('商品组单个详情获取', async () => {

    return requestToApiServer()
      .get(`/product/groupDetail/${productGroup._id}`)
      .expect(200)
      .then(({body}) => {

        expect(body.data.group._id).to.equal(productGroup.id)
      })
  })

  it('商品组详情', async () => {

    return requestToApiServer()
      .get('/product/group_detail')
      .expect(200)

  })

  it('按的准商品组', async () => {

    return requestToApiServer()
      .get('/product/pressGroup_detail')
      .expect(200)

  })

  it('积分商城获取', async () => {

    await requestToApiServer()
      .get('/product/point')
      .expect(200)
      .then(res=>{
        expect(res.body).to.have.properties({
          ok: true, data: {
          }
        })
      })

  })

  it('积分商城购买', async () => {

    const point = 9999
    await PlayerModel.findByIdAndUpdate(player, {$set: {point}})
    await requestToApiServer()
      .post(`/product/pointBuy/${pointP.id}`)
      .set({'x-jwt': jwt})
      .expect(200)
      .then(res => {
        expect(res.body).to.have.properties({
          ok: true, data: {}
        })
      })

    const playerAfter = await PlayerModel.findById(player)
    expect(playerAfter.point).to.equal(point - pointP.pointPrice)

    const prize = await GamePrizeModel.findOne({product: pointP.id, player: player.id}).sort({createAt: -1})
    expect(prize.from).to.equal('pointBuy')

  })

})
