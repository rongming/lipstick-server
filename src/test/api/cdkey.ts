import * as request from 'supertest'
import {PlayerModel} from "../../database/models/player";
import app from '../../serverApp'
import {expect, setupTestEnv, tearDownTestEnv, TestEnv} from "./helper";

describe('cdkey api', () => {

  let player = null
  let env: TestEnv = null
  let jwt = null
  let cdkey = null

  before(async () => {
    env = await setupTestEnv()
    player = env.player
    jwt = env.jwt
  })

  after(async () => {
    await tearDownTestEnv()
  })

  context('一对多CDKEY', async function () {
    beforeEach(() => {
      cdkey = env.cdkeyOneToAll
    })
    it('使用', async function () {

      const p0 = await PlayerModel.findById(player.id)
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: true
          })
        })

      const p1 = await PlayerModel.findById(player.id)
      expect(p1.freeCoin).to.equal(p0.freeCoin + cdkey.freeCoin)
      expect(p1.coin).to.equal(p0.coin + cdkey.coin)
      expect(p1.gem).to.equal(p0.gem + cdkey.gem)
      expect(p1.point).to.equal(p0.point + cdkey.point)
    });

    it('别人可以使用', async function () {
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': env.jwtPoor})
        .then(res => {
          expect(res.body).have.properties({
            ok: true
          })
        })
    })

    it('自己无法重复使用', async function () {
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: false
          })
        })
    })
  })

  context('一对特殊群体CDKEY', async function () {
    beforeEach(() => {
      cdkey = env.cdkeyOneToSome
    })
    it('别人无法使用', async function () {
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': env.jwtPoor})
        .then(res => {
          expect(res.body).have.properties({
            ok: false
          })
        })
    })
    it('自己能使用', async function () {

      const p0 = await PlayerModel.findById(player.id)
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: true
          })
        })

      const p1 = await PlayerModel.findById(player.id)
      expect(p1.freeCoin).to.equal(p0.freeCoin + cdkey.freeCoin)
      expect(p1.coin).to.equal(p0.coin + cdkey.coin)
      expect(p1.gem).to.equal(p0.gem + cdkey.gem)
      expect(p1.point).to.equal(p0.point + cdkey.point)
    });

    it('自己无法再无法使用', async function () {
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: false
          })
        })
    })
  })

  context('多对多CDKEY', async function () {
    beforeEach(() => {
      cdkey = env.cdkeyAllToAll
    })
    it('使用', async function () {

      const p0 = await PlayerModel.findById(player.id)
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: true
          })
        })

      const p1 = await PlayerModel.findById(player.id)
      expect(p1.freeCoin).to.equal(p0.freeCoin + cdkey.freeCoin)
      expect(p1.coin).to.equal(p0.coin + cdkey.coin)
      expect(p1.gem).to.equal(p0.gem + cdkey.gem)
      expect(p1.point).to.equal(p0.point + cdkey.point)
    });

    it('用过后key就该不存在', async function () {
      await request(app)
        .post(`/player/cdkey/${cdkey.keys[0]}`)
        .set({'x-jwt': jwt})
        .then(res => {
          expect(res.body).have.properties({
            ok: false
          })
        })
    })
  })
})
