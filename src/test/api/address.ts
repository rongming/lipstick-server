import {AddressModel} from "../../database/models/address";
import {expect, requestToApiServer, setupTestEnv, tearDownTestEnv, TestEnv} from "./helper";

describe('地址API', () => {

  let env: TestEnv = null

  before(async () => {
    env = await setupTestEnv()
    return await AddressModel.deleteMany({})
  })

  after(async () => {
    await tearDownTestEnv()
  })

  afterEach(async () => {
    return await AddressModel.deleteMany({})
  })

  it('获取自己的地址', async () => {

    await requestToApiServer()
      .get('/player/address')
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({
          ok: true,
          data: {
            addresses: []
          }
        })
      })
  })

  it('pca码查询街道', async () => {

    await requestToApiServer()
      .get('/player/address/streets')
      .query({pca: '330208'})
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({
          ok: false, error: "BAD_PCA_CODE",
          msg: "错误的PCA码"
        })
      })


    await requestToApiServer()
      .get('/player/address/streets')
      .query({pca: '330108'})
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({
          ok: true,
          data: {
            streets: [{
              "code": "330108001",
              "name": "西兴街道"
            },
              {
                "code": "330108002",
                "name": "长河街道"
              },
              {
                "code": "330108003",
                "name": "浦沿街道"
              }]
          }
        })
      })
  })


  it('用户创建地址', async () => {
    await requestToApiServer()
      .post('/player/address')
      .set({'x-jwt': env.jwt})
      .send({
        pcas: '330108003',
        address: '保亿大厦1111室',
        receiver: 'pshu',
        phone: '18688887777',
        alias: '公司',
      })
      .expect(200)

    await requestToApiServer()
      .get('/player/address')
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({
          ok: true,
          data: {
            addresses: [{player: env.player.id, default: true}]
          }
        })
      })
  })

  async function createAddresses(n: number) {

    for (let i = 1; i <= n; i++) {

      await requestToApiServer()
        .post('/player/address')
        .set({'x-jwt': env.jwt})
        .send({
          pcas: '330108003',
          address: '保亿大厦1111室',
          receiver: 'pshu',
          phone: '18688887777',
          alias: `公司${n}`,
          default: true
        })
        .expect(200)
    }
  }


  it('创建地址,设置默认地址', async () => {

    await createAddresses(2)

    await requestToApiServer()
      .get('/player/address')
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        const defaultAdd = body.data.addresses.find((add) => add.default)
        expect(defaultAdd).to.have.properties({alias: '公司2'})
      })
  })

  it('更新地址', async () => {

    await createAddresses(1)

    let address = null
    await requestToApiServer()
      .get('/player/address')
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        [address] = body.data.addresses
      })

    await requestToApiServer()
      .patch(`/player/address/${address._id}`)
      .set({'x-jwt': env.jwt})
      .send({
        pcas: '330108003',
        address: '保亿大厦3333室',
        receiver: 'qshu',
        phone: '18688889999',
        alias: `公司X`,
      })
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({ok: true})
      })
  })

  it('修改已有地址为默认地址', async () => {

    await createAddresses(2)

    let addresses = []
    await requestToApiServer()
      .get('/player/address')
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        addresses = body.data.addresses
      })

    const nonDefaultAddress = addresses.find(a => !a.default)

    await requestToApiServer()
      .put(`/player/address/${nonDefaultAddress._id}/default`)
      .set({'x-jwt': env.jwt})
      .send()
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({ok: true})
      })
  })

  it('删除地址', async () => {

    let address = null
    await requestToApiServer()
      .post('/player/address')
      .set({'x-jwt': env.jwt})
      .send({
        pcas: '330108003',
        address: '保亿大厦2222室',
        receiver: 'pshu',
        phone: '18688887777',
        alias: '公司2',
        default: true
      })
      .expect(200)
      .then(({body}) => {
        address = body.data.address
      })

    await requestToApiServer()
      .delete(`/player/address/${address._id}`)
      .set({'x-jwt': env.jwt})
      .expect(200)
      .then(({body}) => {
        expect(body).to.have.properties({ok: true})
      })

  })
})
