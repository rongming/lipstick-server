import * as config from 'config'
import * as mongoose from 'mongoose'
import GlobalModel from "../../database/models/global";


before(async () => {
  await mongoose.connect(config.get('database.url'), {useNewUrlParser: true})

  await GlobalModel.deleteMany({})

  return GlobalModel.create({_id: "testGlobal", shortIdCounter: 200000})
})

after(() => {
  return mongoose.disconnect()
})
