import {InstanceType} from "typegoose";
import {DeliveryModel} from "../../database/models/delivery";
import {DeliveryOrderModel} from "../../database/models/deliveryOrder";
import {GameActivityModel} from "../../database/models/gameActivity";
import {Product, ProductModel} from "../../database/models/Product";

import {expect, requestToApiServer, setupTestEnv, tearDownTestEnv, TestEnv} from "./helper";

describe('运单API', () => {

  let env: TestEnv = null

  before(async () => {
    env = await setupTestEnv()
  })
  after(async () => {
    await tearDownTestEnv()
  })

  context('无运单', () => {

    it('获取', () => {

      return requestToApiServer()
        .get('/delivery')
        .set({'x-jwt': env.jwt})
        .expect(200)
        .then(({body}) => {

          expect(body).to.have.properties({
            ok: true, data: {
              delivery: []
            }
          })
        })
    })
  })

  context('有运单', () => {
    let address = null
    let prizes = null
    let mostStockProduct = null
    let otherStockProduct = null

    beforeEach(async () => {

      const ps = env.lipstickSeries.products as Array<InstanceType<Product>>
      const products = [] as Array<InstanceType<Product>>
      for (const p of ps) {
        const product = await ProductModel.findById(p._id)
        if (product)
          products.push(product)
      }
      products.sort((a, b) => {
        return b.stock - a.stock
      })
      mostStockProduct = products[0]
      otherStockProduct = products[1]

      const {body} = await requestToApiServer()
        .post(`/game/start/${env.lipstickSeries.id}`)
        .set({'x-jwt': env.jwt})
        .expect(200)
        .send()

      const act = body.data.activity

      await GameActivityModel.findByIdAndUpdate(act, {$set: {canWin: true}})

      await requestToApiServer()
        .post(`/game/claim/${act}`)
        .set({'x-jwt': env.jwt})
        .expect(200)
        .send({win: true, stage: 0})
        .then(({body}) => {
          expect(body).to.have.properties({data: {win: true}})
        })

      await requestToApiServer()
        .post('/player/address')
        .set({'x-jwt': env.jwt})
        .send({
          pcas: '330108003',
          address: '保亿大厦2222室',
          receiver: 'pshu',
          phone: '18688887777',
          alias: '公司2',
          default: true
        })
        .expect(200)
        .then(({body}) => {
          address = body.data.address
        })

      await requestToApiServer()
        .get('/player/prize')
        .set({'x-jwt': env.jwt})
        .expect(200)
        .then(({body}) => {
          prizes = body.data.prizes
        })

      await requestToApiServer()
        .post(`/player/prize/deliver/delivery`)
        .set({'x-jwt': env.jwt})
        .send({toAddress: address._id, prizeIds: [prizes[0]._id]})
        .then(({body}) => {
          expect(body).to.have.properties({
            ok: true,
          })
        })
    })

    afterEach(async () => {
      await GameActivityModel.deleteMany({})
      await DeliveryOrderModel.deleteMany({})
      await DeliveryModel.deleteMany({})
    })

    it('获取', () => {

      return requestToApiServer()
        .get('/delivery')
        .set({'x-jwt': env.jwt})
        .expect(200)
        .then(({body}) => {

          expect(body).to.have.properties({
            ok: true, data: {}
          })
        })
    })

  })

})
