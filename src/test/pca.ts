import {findAreaByPcaCode, findStreetByPcasCode, isValidatePcaCode, isValidatePcasCode} from "../pcas/pca2pacs";
import {expect} from "./api/helper";

describe('行政区编码', () => {

  it('findAreaByPcaCode: pca 2 streets', () => {
    expect(findAreaByPcaCode('330108')).to.have.properties({
      name: '滨江区',
      children: [
        {code: '330108001', name: '西兴街道'},
        {code: '330108002', name: '长河街道'},
        {code: '330108003', name: '浦沿街道'}
      ]
    })
  })

  it('findAreaByPcaCode: pcas 2 street', () => {

    expect(findStreetByPcasCode('330108003')).to.have.properties(
      {code: '330108003', name: '浦沿街道'}
    )
  })

  it('isValidatePcaCode', () => {
    expect(isValidatePcaCode('330108')).to.be.true

    expect(isValidatePcaCode('3301081')).to.be.false
    expect(isValidatePcaCode('330199')).to.be.false
    expect(isValidatePcaCode('339999')).to.be.false
    expect(isValidatePcaCode('999999')).to.be.false
  })

  it('isValidatePcasCode', () => {
    expect(isValidatePcasCode('330108002')).to.be.true

    expect(isValidatePcasCode('3301080021')).to.be.false
    expect(isValidatePcasCode('330108999')).to.be.false
    expect(isValidatePcasCode('330199999')).to.be.false
    expect(isValidatePcasCode('339999999')).to.be.false
    expect(isValidatePcasCode('999999999')).to.be.false
  })
})
