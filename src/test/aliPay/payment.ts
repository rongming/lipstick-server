import * as fs from "fs";
import ms = require("ms");
import * as config from "config"
import {Types} from "mongoose";
import * as path from "path";
import {AliAuther, AliPayCashier, aliPayServerAuther} from "../../alipay/payment";
import {expect} from "../api/helper";

const appId = config.get<string>("ali.app_id")
const notifyUrl = config.get<string>("ali.notify_url")

const keyPathBase = path.join(__dirname, '..', '..', '..', 'alipay')

const merchantAuther = new AliAuther(
  fs.readFileSync(path.join(keyPathBase, 'rsa_private_key.pem'), 'utf-8'),
  fs.readFileSync(path.join(keyPathBase, 'rsa_public_key.pem'), 'utf-8')
)

const alipayCashier = new AliPayCashier(
  appId, merchantAuther, merchantAuther,
  notifyUrl,
)


const mockAliPayCashier = new AliPayCashier(
  appId, merchantAuther, merchantAuther, notifyUrl
)


describe('阿里支付', () => {


  context('签名安全', () => {


    it('签名', () => {
      expect(merchantAuther.sign({"a": "123"}))
        .to.equal(`eCkSHF312/b19+GKBMCIsht0wa3GDhLOG3LS7UeK8fSUKmxbUDT0J9ZLWiNop6o2iM9U4Qi8gJWlZ4Ilyg8WMUswLVK54MLOK3/CNNtp8V8H8D8jBtbnBK41xYDbpiagOmGgXW4hq9kA0AfAlWLt77F8zDCw0vDZ+t9Q4SNDmq7tTZOB/e0VfMAYFkEDKoHM/iX6B3kkMjV7gvf0sOPc6oqMrB5LoMdQhMCNcukUGwwbR1BhrVBVaS1qZ4rMupz6K9Ey9HdsYP8kalrtp57XnrJzA0NB5N/LELs6pCvP9EM5KenjfkPDJ0Gnh4sHCm5ysKp5v6N6/jAVsPcKM0L3eQ==`)
    })

    it('验证签名', () => {

      const res = {
        gmt_create: '2019-04-01 15:05:00',
        charset: 'utf-8',
        seller_email: 'gonglong_1412@51ulong.com',
        subject: '购买商品',
        sign:
          'dXOTmo1rZSy8UvfCIGmGNMIKrbUx5ghWtKuXLOZ7+GWi1JwGwJq4EExdQ+0gY6jbdtfBjkAiVJP6eQ77usm/mD7kmDIelhQazU6wK+cH2xtJOlBRkCfvvCQujkgKWRAR7MasmjXSDQde3nlTo32JW7YyJxj9Tz/A81JMY1QGqAlKHuKHt8tMoJykD+CNMvIzI/Vy/rCnSfqo2ueKHRP7QvvVxA/a9jIvDttNnDmHkiFQWntGqHJONirQLtnYL0YWyIYNT/I3sXx9DCGG0aVn8rAKkjw+6VrROmgONWBaCmq4MDqjb5/7m1InQv+pXEvxUS+Aod18V9QLi2HpWcTojg==',
        buyer_id: '2088202757477872',
        invoice_amount: '0.01',
        notify_id: '2019040100222150501077871003702773',
        fund_bill_list: '[{"amount":"0.01","fundChannel":"ALIPAYACCOUNT"}]',
        notify_type: 'trade_status_sync',
        trade_status: 'TRADE_SUCCESS',
        receipt_amount: '0.01',
        app_id: '2019032563729047',
        buyer_pay_amount: '0.01',
        sign_type: 'RSA2',
        seller_id: '2088221764801041',
        gmt_payment: '2019-04-01 15:05:01',
        notify_time: '2019-04-01 15:08:21',
        version: '1.0',
        out_trade_no: '5ca1b818f719f011c9aa56bb',
        total_amount: '0.01',
        trade_no: '2019040122001477871028211766',
        auth_app_id: '2019032563729047',
        buyer_logon_id: 'nic***@163.com',
        point_amount: '0.00'
      }

      expect(aliPayServerAuther.verify(res)).to.be.true

      // tiny modification leads to verify failure
      res.total_amount = '0.02'
      expect(aliPayServerAuther.verify(res)).to.be.false
    })


  })

  it('下单', async () => {
    const order = Types.ObjectId().toString()

    const res = await alipayCashier.createPaymentOrder({
      type: 'ProductOrder',
      order,
      title: '测试',
      detail: '测速TDD',
      fromIP: '127.0.0.1',
      price: 1.11
    }, ms('15m'))
  })

  it('确认订单', () => {

  })

})
