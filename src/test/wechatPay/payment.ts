import {Types} from "mongoose";
import {PaymentOrderModel} from "../../database/models/paymentOrder";
import {wechatPayAuther, wechatPayCashier} from "../../wechat/wechatCashier";
import ms = require("ms");
import {expect} from "../api/helper";


describe('微信支付', () => {

  it('创建订单', async () => {
    const payOrder = await wechatPayCashier.createPaymentOrder({
      type: 'test',
      order: Types.ObjectId().toString(),
      fromIP: '127.0.0.1',
      title: '测试物品',
      detail: '测试物品 666',
      price: 9.99
    }, ms('15min'))
  })


  it('确认支付', async () => {

    const orderId = Types.ObjectId().toString()

    const payOrder = await wechatPayCashier.createPaymentOrder({
      type: 'test',
      order: orderId,
      fromIP: '127.0.0.1',
      title: '测试物品',
      detail: '测试物品 666',
      price: 10.24
    }, ms('15min'))


    const dispatcher = {
      async confirm(pay) {
        expect(pay).to.have.properties({orderType: 'test', order: orderId})
      }
    }

    const confirm = {
      out_trade_no: payOrder.paymentOrder.id,
      result_code: 'SUCCESS', return_code: 'SUCCESS',
    }

    const sign = wechatPayAuther.sign(confirm)

    await wechatPayCashier.confirmPayment({...confirm, sign}, dispatcher)

    const paymentOrder = await PaymentOrderModel.findById(payOrder.paymentOrder.id)

    expect(paymentOrder).to.have.properties({state: 'finished'})
  })

})
