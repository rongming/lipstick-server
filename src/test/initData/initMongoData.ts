import {MailType} from "../../database/models/mail";
import {
  testCdkey,
  testLipstickSeries,
  testLittleGift,
  testPointMarketProduct,
  testPressGroup,
  testProduct,
  testRecharge
} from "./data";
import ms = require("ms");

export const initMongoProduct = {
  ...testProduct,
  name: `三百元测试商品`,
  coverUrl: "http://t.cn/EXfI3Ic",
  marketPrice: 300,
  underLinePrice: 10000,
  stock: 500,
  promotePrice: 1,
}

export const initMongoPointProduct = {
  ...testPointMarketProduct,
  name: '积分商城商品',
  onPointMarket: true,
  pointPrize: 5
}

export const initMongoLittleGift = {
  ...testLittleGift,
  name: `三百元测试商品`,
  coverUrl: "http://t.cn/EXfI3Ic",
  marketPrice: 300,
  stock: 400,
}

export const initMongoRecharge = {
  ...testRecharge,
  RMBCost: 0.01,
  coin: 100,
  freeCoin: 10,
  gem: 0,
}

export const initMongoLipstickSeries = {
  ...testLipstickSeries,
  priceInCoin: 0.1,
  coverUrl: "http://t.cn/EXfI3Ic",
  details: ["http://t.cn/EXfI3Ic", "http://t.cn/EXfI3Ic", "http://t.cn/EXfI3Ic", "http://t.cn/EXfI3Ic"]
}

export const initMongoPressGroup = {
  ...testPressGroup,
}

export const initMongoCdkey = {
  ...testCdkey,
  expireAt: new Date(Date.now() + ms(`3years`)),
}

export const initMongoPublicMail = {
  title: `服务器补偿维护 特地放出一些验证码`,
  content: `奖励一万欧元，并且2020年前兑换码中输入hello world还可以兑换钱`,
  type: MailType.NOTICEGIFT,
  gift: {coin: 1000, freeCoin: 2000, gem: 3000, point: 4000}
}
