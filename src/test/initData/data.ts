import {MailType} from "../../database/models/mail";
import {PressGroupModel} from "../../database/models/ProductGroup";
import ms = require("ms");

export const testProduct = {
  "skuId": `${Math.random()}`,
  "brand": "MAC",
  "name": "MAC魅可柔感哑光唇膏 ",
  "marketPrice": 170,
  "coverUrl": "https://res.lipstick.lemiao.xyz/images/lipsticks/kh104.png",
  "category": "lipstick",
  "description": "DIVA 姨妈色",
  "underLinePrice": 3000,
  "tag": "MAC",
  "stock": 5,
  authorized: true,
  expireAt: new Date(),
  createAt: new Date(),
  state: 'on',
  gram: 1,
  wrap: "new",
  otherPrice: {jd: 100, tmall: 200, kola: 100},
  model: 'chili',
  series: 'matte',
  promotePrice: 10,
  onPointMarket: false,
  pointPrice: 9999,
  pointRedeem: 100
}

export const testPointMarketProduct = {
  ...testProduct,
  name: '积分商城商品',
  onPointMarket: true,
  pointPrice: 500,
  pointRedeem: 200
}

export const testLittleGift = {
  "skuId": `${Math.random()}`,
  "brand": "晨光",
  "name": "中性笔 黑色",
  "marketPrice": 3,
  "coverUrl": "https://res.lipstick.lemiao.xyz/images/lipsticks/kh104.png",
  "category": "littleGift",
  "description": "0.38mm 丝滑流畅 写起字来舒服的一批 赶紧来买买买",
  "underLinePrice": 5,
  "tag": "MAC",
  "stock": 5,
  authorized: true,
  expireAt: new Date(),
  createAt: new Date(),
  state: 'on',
  gram: 1,
  wrap: "new",
  otherPrice: {jd: 100, tmall: 200, kola: 100},
  model: 'chili',
  series: 'matte',
  promotePrice: 10,
  onPointMarket: false,
  pointPrice: 50,
  pointRedeem: 30
}

export const testRecharge = {
  "skuId": `${Math.random()}`,
  "name": "test 充值 ",
  "RMBCost": 1,
  "coin": 15,
  "freeCoin": 10,
  "gem": 0,
  state: "on"
}

export const testLipstickSeries = {
  brand: 'mac test LipstickSeries',
  name: 'name testLipstickSeries',
  seriesPrice: 200,
  underLinePrice: 3000,
  priceInCoin: 100,
  coverUrl: "https://res.lipstick.lemiao.xyz/images/lipsticks/kh104.png",
  description: "小辣椒 chili testLipstickSeries",
  onStock: true,
  stock: 0,
  details:
    ["http://t.cn/EXfI3Ic",
      "http://t.cn/EXfI3Ic",
      "http://t.cn/EXfI3Ic",
      "http://t.cn/EXfI3Ic"]
}

export const testPressGroup = {
  name: '按的准 2019-04-26 18:17',
  description: "按的准group描述test",
  priceInCoin: 100,
  priceInGem: 0,
  coverUrl: "https://res.lipstick.lemiao.xyz/images/lipsticks/kh104.png",
  state: 'on'
}

export const testCdkey = {
  expireAt: new Date(Date.now() + ms(`2years`)),
  state: 'on',
  coin: 100,
  freeCoin: 10,
  gem: 70,
  point: 20
}

export const testPublicMail = {
  title: `服务器补偿维护`,
  content: `奖励一万欧元，并且2020年前兑换码中输入hello world还可以兑换钱`,
  type: MailType.NOTICEGIFT,
  gift: {coin: 1, freeCoin: 2, gem: 3, point: 4}
}
