import * as config from "config";
import createAuther from "./auther";
import {WechatCashier} from "./payment";

const appId = config.get<string>("wx.app_id")
const mchId = config.get<string>("wx.mchId")
const secret = config.get<string>("wx.sign_key")
const notifyUrl = config.get<string>("wx.notify_url")
export const wechatPayAuther = createAuther(secret)

export const wechatPayCashier = new WechatCashier(
  appId, mchId,
  notifyUrl, wechatPayAuther
)

