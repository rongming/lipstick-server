export interface IAuther {
  sign(toSingObject: any): string

  verify(toVerifyObject: any): boolean

}


export type PaymentArguments = {
  type: string
  order: string
  price: number
  title: string
  detail: string
  fromIP: string
}

