/**
 * Created by Color on 2016/7/6.
 */
const algorithm = {
  shuffle: function (arr) {
    const array = arr;
    let maxIndex = arr.length - 1;
    while (maxIndex > 0) {
      const randIndex = Math.floor(Math.random() * (maxIndex + 1));
      const t = arr[maxIndex];
      array[maxIndex] = arr[randIndex];
      array[randIndex] = t;
      maxIndex--;
    }
  }
}


/* eslint no-extend-native: "off" */
Array.prototype.contains = function (e) {
  for (let i = 0; i < this.length; i++) {
    if (this[i] === e) {
      return true;
    }
  }
  return false;
};


Array.prototype.remove = function (e) {
  for (let i = 0; i < this.length; i++) {
    if (this[i] === e) {
      this.splice(i, 1);
      return true;
    }
  }
  return false;
};

Array.prototype.removeFilter = function (match) {
  for (let i = 0; i < this.length; i++) {
    if (match(this[i])) {
      this.splice(i, 1);
      return true;
    }
  }
  return false;
};

Array.prototype.filterCount = function (match) {
  let sum = 0;
  for (let i = 0; i < this.length; i++) {
    if (match(this[i])) {
      sum++;
    }
  }
  return sum;
};

Array.prototype.all = function (match) {
  for (let i = 0; i < this.length; i++) {
    if (!match(this[i])) {
      return false;
    }
  }
  return true;
};

export default algorithm;

