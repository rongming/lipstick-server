import * as winston from "winston";

const {format} = winston

export const logger = winston.createLogger({
  level: 'info',
  format: format.combine(format.timestamp(), format.simple()),
  transports: [
    new winston.transports.Console
  ]
});
