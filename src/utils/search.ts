export const searchAry = (ary, regExp, depth = 10) => {
  return ary.filter(o => {
    return iteratorSearch(o, regExp, depth)
  })
}

export const iteratorSearch = (obj, regExp, depth = 10) => {
  // tslint:disable-next-line:forin
  for (const key in obj) {
    // console.log(key, obj[key]);
    depth--;
    if (depth > 0 && typeof (obj[key]) === 'object') {
      if (iteratorSearch(obj[key], regExp))
        return true
    } else {
      if (regExp.test(obj[key]))
        return true
    }
  }
  return false
}
