import * as request from 'superagent'
import * as config from 'config'

export default {getStatus, notice, addResource}

const serverApi = config.get('gm-tool.server.api')

async function getStatus(): Promise<{ players, rooms }> {
  try {
    const res = await request.get(`${serverApi}/status`)
    return res.body.online
  } catch (e) {
    console.log(`${__filename}:13 `, e)
    return {players: 0, rooms: 0}
  }
}


async function notice(message): Promise<boolean> {
  try {
    const res = await request.post(`${serverApi}/notice`).send({notice: message})
    return res.body.ok
  } catch (e) {
    console.log(`${__filename}:24 `, e)
    return false
  }
}

async function addResource(playerId, addGem, addGold): Promise<boolean> {
  try {
    const res = await request.post(`${serverApi}/addResource`).send({
      playerId,
      addGem: parseInt(addGem, 10),
      addGold: parseInt(addGold, 10)
    })
    return res.body.ok
  } catch (e) {
    console.log(`${__filename}:13 `, e)
    return false
  }
}
