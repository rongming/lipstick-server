import * as crypto from  'crypto';
import  {extKey} from 'config';

const secret = extKey

export const sign = function (str) {
  const hash = crypto.createHash('md5')

  hash.update(`${str}=${secret}`)
  return hash.digest('hex')
}


export const verify = function (str) {
  const [toSign, key] = str.split('=')
  const computedKey = sign(toSign, secret)
  return computedKey === key
}
