import * as fs from 'fs'
import * as path from 'path'
import {makeExecutableSchema} from 'graphql-tools'
import {GamePrize} from "../../database/models/gamePrize";


const subSchemas = []

const files = fs.readdirSync(path.join(__dirname, 'nodes'))
// after compile from ts to js , you need to require js file too
  .filter(filename => filename.endsWith('.ts') || filename.endsWith('.js'))
  .map(f => path.join(__dirname, 'nodes', f))


files.map(f => subSchemas.push(require(f)))


const allTypeDefs = subSchemas.map((ss) => ss.typeDefs).join('\n')
const allQueryDefs = `
type Query{
  ${subSchemas.map(ss => ss.queryDefs).join('\n')}
}
`
const allMutationsDefs = `
  type Mutation{
    ${subSchemas.map(ss => ss.mutationDefs).join('\n')}
  }
`
const AllQuery = {}
const AllMutation = {}
const FieldResolvers = {}

subSchemas.forEach(({resolvers: {Query, Mutation, ...Other}}) => {
  Object.assign(AllQuery, Query)
  Object.assign(AllMutation, Mutation)
  Object.assign(FieldResolvers, Other)
})

const schemaDef = [allTypeDefs, allQueryDefs, allMutationsDefs]
const resolvers = {
  Query: AllQuery, Mutation: AllMutation, ...FieldResolvers
}

export default makeExecutableSchema({typeDefs: schemaDef, resolvers})
