import serverApi from '../deps/server-api'

const typeDef = `
  type Mutation{
      addNotice(notice:String) : Boolean
  }
`

const Mutation = {
  async addNotice(_, {notice}) {
    return await serverApi.notice(notice)
  }
}

export {typeDef, Mutation}
