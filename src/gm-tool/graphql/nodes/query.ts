import serverApi from '../../deps/server-api'
import {PlayerModel} from '../../../database/models/player'

export const typeDefs = `
type Status {
  players:Int
  rooms:Int
}

type PlayerStat{
  allPlayers:Int!
  wechatPlayers:Int!
}

type PlayersResult{
   players: [Player]
   pageCount: Int
}
`

export const queryDefs = `
  status: Status
  playerStat: PlayerStat
  wechatPlayers(page:Int!):PlayersResult
`


export const resolvers = {
  Query: {
    async status() {
      return await serverApi.getStatus()
    },
    async wechatPlayers(_, {page}) {
      const players = await PlayerModel.find({isTourist: false})
        .sort({createAt: -1})
        .skip(page * 10)
        .limit(10)
        .lean()
        .exec()

      const playerTotal = await PlayerModel.count({isTourist: false})
      const pageCount = Math.ceil(playerTotal / 10)
      return {players, pageCount}
    },

    async playerStat() {
      const [allPlayers, wechatPlayers] = await [PlayerModel.count({}), PlayerModel.count({isTourist: false})]
      return {allPlayers, wechatPlayers}
    }
  }
}

