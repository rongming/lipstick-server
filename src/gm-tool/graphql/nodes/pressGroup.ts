import {detectStockOfPress} from "../../../api/games/pressGame";
import {LipstickSeriesModel} from "../../../database/models/lipstickSeries";
import {PressGroupModel} from "../../../database/models/ProductGroup";

export const typeDefs = `

type PressGroup{
   _id: String!
   name: String!
   description: String!
   priceInCoin: Int!
   priceInFangka: Int!
   coverUrl: String!
   state: String!
   level1: [Series]!
   level2: [Series]!
   level3: [Series]!
   updateAt: String!
   createAt: String!
   sortNum: Int!
   hotNum: Int!
}

input PressGroupInput{
   _id: String
   name: String!
   description: String!
   priceInCoin: Int!
   priceInFangka: Int!
   coverUrl: String!
   state: String!
   level1: [String]!
   level2: [String]!
   level3: [String]!
   createAt: String!
   updateAt: String!
   sortNum: Int!
   hotNum: Int!
 }
`

export const queryDefs = `
   pressGroups: [PressGroup]!
   searchPressGroups(query:String!): [PressGroup]!
`

export const mutationDefs = `
  createPressGroup(pressGroup:PressGroupInput): PressGroup
  updatePressGroup(_id:String!, pressGroup:PressGroupInput): PressGroup
  deletePressGroup(_id:String!): Boolean
  onPressGroup(_id:String!): PressGroup
  offPressGroup(_id:String!): PressGroup
`

function resolveLevel(name: string) {

  return async (parent, args, context, info) => {
    const seires = []

    for (const id of parent[name]) {
      seires.push(await LipstickSeriesModel.findById(id))
    }

    return seires
  }
}

export const resolvers = {
  Query: {
    pressGroups: async (_) => {
      return PressGroupModel.find().sort({sortNum: -1, createAt: -1})
    },
    searchPressGroups: async (_, {query}) => {
      const reg = new RegExp(query, 'gi')
      return PressGroupModel.find({
        name: reg
      }).lean()
    }
  },
  Mutation: {
    createPressGroup: async (_, {pressGroup}) => {
      const createAt = new Date(pressGroup.createAt)
      const updateAt = new Date(pressGroup.updateAt)
      const pg = new PressGroupModel({...pressGroup, createAt, updateAt})
      await pg.save()
      const onStock = await detectStockOfPress(pg.id)
      if (!onStock)
        throw Error(`该按得准商品组中库存不足每个等级一件(元商品)，已经自动下架`)
      // const pg = await PressGroupModel.create({...pressGroup, createAt, updateAt})
      return pg
    },
    updatePressGroup: async (_, {_id, pressGroup}) => {
      const updateAt = new Date(pressGroup.updateAt)
      const pg = await PressGroupModel.findByIdAndUpdate(_id, {...pressGroup, updateAt}, {new: true})
      const onStock = await detectStockOfPress(pg.id)
      if (!onStock)
        throw Error(`该按得准商品组中库存不足每个等级一件(元商品)，已经自动下架`)
      return pg
    },
    deletePressGroup: async (_, {_id}) => {
      return PressGroupModel.deleteOne({_id})
    },
    onPressGroup: async (_, {_id}) => {
      const pgm = await PressGroupModel.findById(_id)
      const onStock = await detectStockOfPress(pgm.id)
      console.log(onStock)
      if (!onStock)
        throw Error(`该按得准商品组中库存不足每个等级一件(元商品)，已经自动下架`)
      else if (pgm) {
        pgm.state = 'on'
        await pgm.save()
        return pgm
      }
      throw Error('NO_SUCH_PRESS_GROUP')
    },
    offPressGroup: async (_, {_id}) => {
      const pgm = await PressGroupModel.findById(_id)
      if (pgm) {
        pgm.state = 'off'
        await pgm.save()
        return pgm
      }
      throw Error('NO_SUCH_PRESS_GROUP')
    },
  },
  PressGroup: {
    level1: resolveLevel('level1'),
    level2: resolveLevel('level2'),
    level3: resolveLevel('level3')
  }
}
