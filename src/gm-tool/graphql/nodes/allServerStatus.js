import * as request from 'superagent'


const servers = [
  {name: '浦城麻将', url: 'http://mj.xiexiangwangluo.com:9528/public/status'},
  {name: '浦城十三水', url: 'http://sss.xiexiangwangluo.com:9528/public/status'},
  {name: '浦城炸弹', url: 'http://zd.xiexiangwangluo.com:9528/public/status'},
  {name: '跑得快', url: 'http://pdk.xiexiangwangluo.com:9528/public/status'}
]


export const typeDefs = `

  type ServerStatus{
    serverName: String
    onLinePlayers: Int
    rooms: Int
  }
`

export const queryDefs = `

  allServerStatus: [ServerStatus]
`

export const resolvers = {

  Query: {
    async allServerStatus(_) {
      const result = []

      for (const server of servers) {
        const status = await  request(server.url)
        result.push({
          serverName: server.name,
          onLinePlayers: status.body.players,
          rooms: status.body.rooms
        })

      }
      return result

    }
  }
}
