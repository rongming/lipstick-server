import moment = require("moment");
import {GameActivityModel} from "../../../database/models/gameActivity";
import {PlayerModel} from "../../../database/models/player";

export const typeDefs = `
  type GameActivity{
     _id: String!
     player: Player!
     lipstickSeries: Series!
     pressGroup: PressGroup!
     canWin: Boolean
     winLevel: Int
     realWin: Boolean
     realWinLevel: Int
     feeInFangka: Int
     resumeTimes: Int!
     from: String!
     success: Boolean!
     createAt: String!
     endAt: String!
     state: String!
  }
`

export const queryDefs = `
  gameActivityByMonth(month:String!):[GameActivity]!
  gameActivityByRangeAndPlayer(start:String!,end:String!,player:String!):[GameActivity]!
  gameActivityByRangeAndGM(start:String!,end:String!,gm:String!):[GameActivity]!
`

export const mutationDefs = ``

export const resolvers = {
  Query: {
    async gameActivityByMonth(_, {month}) {
      const monthStart = moment(month).startOf('month').toDate()
      const monthEnd = moment(month).endOf('month').toDate()

      return GameActivityModel.find({
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    },

    async gameActivityByRangeAndPlayer(_, {start, end, player}) {
      const monthStart = moment(start).startOf('day').toDate()
      const monthEnd = moment(end).endOf('day').toDate()

      return GameActivityModel.find({
        player,
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    },
    async gameActivityByRangeAndGM(_, {start, end, gm}) {
      const monthStart = moment(start).startOf('day').toDate()
      const monthEnd = moment(end).endOf('day').toDate()

      return GameActivityModel.find({
        relation: gm,
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    }
  },
  Mutation: {},
  GameActivity: {
    async player({player}) {
      const p = await PlayerModel.findById(player)
      if (p) {
        return p
      } else {
        console.log(player)
        return {_id: player}
      }
    }
  }
}
