import * as moment from 'moment'
import {RechargeSummary, RechargeSummaryModel} from '../../../database/models/rechargeSummary'

export const typeDefs = `
  type RechargeSummary{
    _id: String!
    day: String!
    type: String!
    recharges: Int!
    sum: Float!
  }
`

export const queryDefs = `
  listRechargeSummary(day:String): [RechargeSummary]!
`

export const resolvers = {
  Query: {
    async listRechargeSummary(_, {day = new Date()}) {


      const start = moment(day).startOf('month').toDate()
      const end = moment(day).endOf('month').toDate()

      const records = await RechargeSummaryModel.find({day: {$gte: start, $lt: end}})
        .sort({day: -1})
        .limit(100)
        .lean()
      return records
    },
  },
}
