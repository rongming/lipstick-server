import UserRecord from '../../../database/models/userRecord'
import Gm from '../../../database/models/gm'

export const typeDefs = `
  type UserRechargeRecord{
     _id: String!
     from: GM
     to: PlayerInfo! 
     amount: Float!
     relation: [GM]
     created: String!
     source: String!
     currency: String!
     kickback: Float
     kickback2: Float
  }

`

export const queryDefs = `
  userRechargeRecord(page:Int!, gm: String ): [UserRechargeRecord]!
`

export const resolvers = {
  Query: {
    async userRechargeRecord(_, {page, gm}) {

      const requestGm = await Gm.findById({_id: gm})
      if (!requestGm) {
        throw Error('NO SUCH GM')
      }

      const query = requestGm.role === 'super' ? {} : {from: gm}

      const records = await UserRecord.find(query)
        .populate('from to relation')
        .sort({created: -1})
        .skip(10 * page)
        .limit(10)
        .lean()
        .exec()
      records.forEach((r) => {
        r.from = r.from || {username: 'super'}
      })

      return records
    }
  }
}
