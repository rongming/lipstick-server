import moment = require("moment");
import {ConsumeRecordModel} from "../../../database/models/consumeRecord";
import {PlayerModel} from "../../../database/models/player";

export const typeDefs = `
  type ConsumeRecord{
     _id: String!
     player: Player!
     coin: Int
     freeCoin: Int
     gem: Int
     point: Int
     payFor: String!
     success: Boolean!
     createAt: String!
     endAt: String!
     relation: [String]!
  }
`

export const queryDefs = `
  consumeRecordByMonth(month:String!):[ConsumeRecord]!
  consumeRecordByRangeAndPlayer(start:String!,end:String!,player:String!):[ConsumeRecord]!
  consumeRecordByRangeAndGM(start:String!,end:String!,gm:String!):[ConsumeRecord]!
`


export const mutationDefs = ``

export const resolvers = {
  Query: {
    async consumeRecordByMonth(_, {month}) {
      const monthStart = moment(month).startOf('month').toDate()
      const monthEnd = moment(month).endOf('month').toDate()

      return ConsumeRecordModel.find({
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    },

    async consumeRecordByRangeAndPlayer(_, {start, end, player}) {
      const monthStart = moment(start).startOf('day').toDate()
      const monthEnd = moment(end).endOf('day').toDate()

      return ConsumeRecordModel.find({
        player,
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    },
    async consumeRecordByRangeAndGM(_, {start, end, gm}) {
      const monthStart = moment(start).startOf('day').toDate()
      const monthEnd = moment(end).endOf('day').toDate()

      return ConsumeRecordModel.find({
        relation: gm,
        createAt: {$gt: monthStart, $lt: monthEnd},
      }).sort({createAt: -1})
    }
  },
  Mutation: {},
  ConsumeRecord: {
    async player({player}) {
      const p = await PlayerModel.findById(player)
      if (p) {
        return p
      } else {
        console.log(player)
        return {_id: player}
      }
    }
  }
}
