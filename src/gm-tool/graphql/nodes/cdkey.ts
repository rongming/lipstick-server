import {mkCdKeys} from "../../../api/utils";
import {CdkeyModel} from "../../../database/models/cdkey";
import {CdkeyRecordModel} from "../../../database/models/cdkeyRecord";
import {LipstickSeriesModel} from "../../../database/models/lipstickSeries";

export const typeDefs = `

type Cdkey{
   _id: String!
   name: String!
   keys: [String]!
   type: String!
   toPlayers: [Player]!
   coin: Int!
   freeCoin: Int!
   gem: Int!
   point: Int!
   createAt: String!
   updateAt: String!
   expireAt: String!
   state: String!
   remarks: String
}

type CdkeyRecord{
   _id: String!
   cdkey: Cdkey!
   player: Player!
   keyWord: String!
   coin: Int!
   freeCoin: Int!
   gem: Int!
   point: Int!
   createAt: String!
}

input CdkeyInput{
   name: String!
   keys: [String]!
   type: String!
   toPlayers: [String]!
   coin: Int!
   freeCoin: Int!
   gem: Int!
   point: Int!
   createAt: String!
   updateAt: String!
   expireAt: String!
   state: String!
   remarks: String!
   randomSet: Boolean!
   randomNum: Int
}
`

export const queryDefs = `
   cdkeys: [Cdkey]!
   cdkeyRecords(page: Int, size: Int, searchText: String): [CdkeyRecord]!
`

export const mutationDefs = `
  createCdkey(cdkey:CdkeyInput): Cdkey
  updateCdkey(_id:String!, cdkey:CdkeyInput): Cdkey
  deleteCdkey(_id:String!): Boolean
  onCdkey(_id:String!): Cdkey
  offCdkey(_id:String!): Cdkey
`

function resolveLevel(name: string) {

  return async (parent, args, context, info) => {
    const seires = []

    for (const id of parent[name]) {
      seires.push(await LipstickSeriesModel.findById(id))
    }

    return seires
  }
}

export const resolvers = {
  Query: {
    cdkeys: async (_) => {
      return CdkeyModel.find().sort({state: -1, expireAt: -1})
    },
    cdkeyRecords: async (_, {page = 0, size = 20, searchText}) => {
      const crs = await CdkeyRecordModel.find().sort({
        createAt: -1
      }).populate(`cdkey`)
        .populate(`player`)
        .skip(page * size).limit(size).lean()
      return crs
    }
  },
  Mutation: {
    createCdkey: async (_, {cdkey}) => {
      const createAt = new Date(cdkey.createAt)
      const updateAt = new Date(cdkey.updateAt)
      let keys = null
      let key = null
      if (cdkey.randomSet && cdkey.randomNum > 0) {
        keys = mkCdKeys(cdkey.randomNum)
        key = new CdkeyModel({...cdkey, keys, createAt, updateAt})
      } else
        key = new CdkeyModel({...cdkey, createAt, updateAt})
      await key.save()
      return key
    },
    updateCdkey: async (_, {_id, cdkey}) => {
      const updateAt = new Date(cdkey.updateAt)
      const key = await CdkeyModel.findByIdAndUpdate(_id, {...cdkey, updateAt}, {new: true})
      return key
    },
    deleteCdkey: async (_, {_id}) => {
      return CdkeyModel.deleteOne({_id})
    },
    onCdkey: async (_, {_id}) => {
      const key = await CdkeyModel.findById(_id)
      if (key) {
        key.state = 'on'
        await key.save()
        return key
      }
      throw Error('NO_SUCH_PRESS_GROUP')
    },
    offCdkey: async (_, {_id}) => {
      const key = await CdkeyModel.findById(_id)
      if (key) {
        key.state = 'off'
        await key.save()
        return key
      }
      throw Error('NO_SUCH_PRESS_GROUP')
    },
  },
  PressGroup: {
    level1: resolveLevel('level1'),
    level2: resolveLevel('level2'),
    level3: resolveLevel('level3')
  }
}
