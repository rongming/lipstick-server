import * as session from 'express-session';
import * as mongoDBSession from 'connect-mongodb-session'
import * as config from 'config'
import * as Promise from 'bluebird'


const MongoDBStore = mongoDBSession(session)
export default {
  store: null,
  getInstance: function () {
    if (this.store === null) {
      this.store = new MongoDBStore({
        uri: config.get('database.url'),
        collection: 'gmSessions'
      })
    }

    Promise.promisifyAll(this.store.__proto__)
    return this.store
  }
}



