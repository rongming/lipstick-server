import {Channel, Connection} from 'amqplib'
import * as config from 'config'
import * as EventEmitter from 'events'
import {pick} from 'lodash'
import * as uuid from 'uuid'
import * as ws from 'ws'
import {PlayerModel, Player} from '../database/models/player'
import {InstanceType} from 'typegoose'
import {GameTypes} from "../match/gameTypes"
import {deserializeMessage, serializeMessage} from '../network/utils'
import {logger} from "../utils/logger";
import createClient from "../utils/redis"
import {IPlayerModel, ISocketPlayer} from "./ISocketPlayer"
import accountHandlers from './message-handlers-rmq/account'
import chatHandlers from './message-handlers-rmq/chat'
import errorHandlers from './message-handlers-rmq/error'
import gameHandlers from './message-handlers-rmq/game'
import mailHandlers from './message-handlers-rmq/mail';
import {createHandler as createTournamentHandler} from './message-handlers-rmq/tornament';
import {createHandler} from './message-handlers-rmq/match'
import resourceHandlers from './message-handlers-rmq/resource'
import socialHandlers from './message-handlers-rmq/social'


let messageHandlers: object = {
  'test/echo': (player, message) => {
    player.sendMessage('test/echo', message)
  },
}

const ipReg = /(\d+\.\d+\.\d+.\d+)/

const NullSocket = {
  close() {
  },
  terminate() {
  }
}


const rediClient = createClient()

const matchHandlers = createHandler(rediClient)
const tournamentHandler = createTournamentHandler(rediClient)

Object.assign(
  messageHandlers,
  accountHandlers,
  matchHandlers,
  tournamentHandler,
  resourceHandlers,
  gameHandlers,
  socialHandlers,
  chatHandlers,
  errorHandlers,
  mailHandlers
)

export default class SocketPlayer extends EventEmitter implements ISocketPlayer {
  connection: Connection
  private isDone: boolean
  private readonly sendCallback: (err) => any
  private socket: any
  private channel: Channel
  private readonly debugMessage: boolean

  socketId: string
  location: string
  currentRoom: string
  clubId: number
  model: InstanceType<Player>
  gameName: GameTypes

  constructor(socket, connection) {
    super()
    this.isDone = false
    socket.player = this
    this.socket = socket
    this.connection = connection
    this.channel = null
    this.sendCallback = (err) => {
      if (err) {
        logger.warn(err)
      }
    }
    this.debugMessage = config.get('debug.message') || false
    this.location = null
    this.socketId = uuid()
    logger.debug({message: 'socket start ===>', socketId: this.socketId})


    this.getLocation((data) => {
      try {
        const obj = JSON.parse(data)
        if (obj instanceof Object) {
          this.location = `${obj.province || ''}${obj.city || ''}`
        } else {
          this.location = '本地'
        }
      } catch (error) {
        this.location = '本地'
      }
    })
  }

  getDebugMessage(data) {
    let content = data
    if (content.length > 1024) {
      content = `${content.slice(0, 1024)}...`
    }

    return content
  }

  getLocation(onGetData) {
    // const options = {
    //   hostname: 'int.dpool.sina.com.cn',
    //   port: 80,
    //   path: `/iplookup/iplookup.php?format=json&ip=${this.getIpAddress()}`,
    //   method: 'GET',
    //   headers: {
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //   },
    // }
    // http.get(options, (res: IncomingMessage) => {
    //   logger.log('STATUS:', `${res.statusCode}`)
    //   logger.log('HEADERS:', JSON.stringify(res.headers))
    //   res.setEncoding('utf8')
    //   res.on('data', onGetData)
    //   res.on('error', (err) => {
    //     logger.error('RESPONSE ERROR:', err)
    //   })
    // }).on('error', (err) => {
    //   logger.error('socket error', err)
    // })
  }

  getIpAddress() {

    if (!this.socket || !this.socket.remoteAddress) {
      return 'ip获取中'
    }
    const fullAddress = this.socket.remoteAddress || '0.0.0.0'
    const matches = fullAddress.match(ipReg)
    if (matches) {
      return matches[1]
    } else {
      logger.error('the wrong ip is ', fullAddress)
      return 'ip获取中'
    }
  }

  onMessage(data) {
    try {
      if (this.debugMessage) {
        logger.warn(`Player: [${this._id}, ${this.name}] receive ${this.getDebugMessage(data)}`)
      }

      const packet = deserializeMessage(data)
      const handler = messageHandlers[packet.name]
      if (handler) {
        return handler(this, packet.message)
      }

      this.requestToCurrentRoom(packet.name, packet.message)

      logger.error(`未知的消息：${JSON.stringify(packet)}`)
    } catch (e) {
      logger.error(e)
    }

    return false
  }

  get _id() {
    return this.model && this.model._id
  }

  get name() {
    return this.model && this.model.name
  }

  get gold() {
    return (this.model && this.model.gold) || 0
  }

  addGold(v) {
    if (this.model) {
      let g = this.model.gold
      g += v
      this.model.gold = g
      this.sendMessage('resources/updateGold', {gold: g})
      PlayerModel.update({_id: this.model._id}, {$set: {gold: g}},
        (err) => {
          if (err) {
            logger.error(err)
          }
        })
    }
  }

  onDisconnect() {
    this.emit('disconnect', {from: this._id})

    this.socket.player = null
    this.socket = NullSocket
    this.disconnect()
      .catch(error => {
        logger.error('onDisconnect', `Player [${this._id} with error`, error.stack)
      })
      .then(() => {
        logger.info(`Player [${this._id}, ${this.name}] disconnected`)
      })
  }

  async disconnect() {
    this.isDone = true
    logger.info(`Disconnect player: ${this._id}`, this.socketId)

    if (this.socket) {
      const promise = new Promise((resolve) => {
        this.once('disconnect', () => resolve())
      })

      rediClient.decrAsync(`gameCounter.${this.gameName}`)
        .then()
      this.socket.close()
      this.socket.terminate()
      await promise
    }
  }


  sendMessage(name, message) {
    try {
      const packet = {name, message}
      const data = serializeMessage(packet)
      if (this.debugMessage) {
        logger.debug(`Player: [${this._id}, ${this.name}] send ${this.getDebugMessage(data)}`)
      }

      if (this.socket && this.socket.readyState === ws.OPEN) {
        this.socket.send(data, this.sendCallback)
      }
    } catch (e) {
      logger.error(e)
    }
  }

  getGameMsgHandler() {
    return gameHandlers
  }

  isRobot() {
    return false
  }

  get myQueue() {
    return this.socketId
  }

  async connectToBackend(gameName: GameTypes) {

    rediClient.incrAsync(`gameCounter.${this.gameName}`)
      .then()

    this.channel = await this.connection.createChannel()
    this.channel.on('error', error => {
      logger.error('connectToBackend channel error ', this.socketId, error)
      try {
        this.socket.close()
        this.socket.terminate()
      } catch (closingSocketError) {
        logger.error('terminating socket error', this.socketId, closingSocketError)
      }
    })

    await this.channel.assertExchange('userCenter', 'topic', {durable: false})
    await this.channel.assertQueue(this.myQueue, {exclusive: true, durable: false, autoDelete: true})

    try {
      await this.channel.bindQueue(this.myQueue, 'userCenter', `user.${this._id}`)
      await this.channel.bindQueue(this.myQueue, 'userCenter', `user.${this._id}.${gameName}`)
      const {consumerTag} = await this.channel.consume(this.myQueue, async (message) => {
        if (!message) return


        try {
          const messageBody = JSON.parse(message.content.toString())

          logger.info(`from ${gameName} [${this.currentRoom}] to ${this._id}`, messageBody.name || messageBody.cmd)

          if (messageBody.type === 'cmd' && messageBody.cmd === 'leave' && this.socketId !== messageBody.sid) {
            this.socket.close()
            this.socket.terminate()
            await this.channel.cancel(consumerTag)
            return this.channel.close()
          }

          if (messageBody.name === 'room/join-success') {
            this.currentRoom = messageBody.payload._id
            this.cancelListenClub(this.clubId)
          }

          if (messageBody.name === 'newClubRoomCreated') {
            this.sendMessage('club/updateClubInfo', messageBody.payload)
            return;
          }

          if (messageBody.name === 'clubRequest') {
            this.sendMessage('club/haveRequest', {})
            return;
          }


          if (messageBody.name === 'resources/updateGold') {
            this.updateGoldGemRuby({gold: messageBody.payload.gold})
          }
          if (messageBody.name === 'resource/createRoomUsedGem') {
            this.updateGoldGemRuby({gem: -messageBody.payload.createRoomNeed})
          }

          this.sendMessage(messageBody.name, messageBody.payload)
        } catch (err) {
          logger.error('backMessageFromBackend', err, message.content.toString())
        }
      }, {noAck: true})

      this.channel.publish('userCenter', `user.${this._id}`, new Buffer(
        JSON.stringify({type: 'cmd', cmd: 'leave', sid: this.socketId})))

    } catch (e) {
      logger.error('consume error', this.socketId, e)
    }
  }

  async listenClub(clubId = -1) {
    if (this.channel && clubId) {
      await this.channel.assertExchange(`exClubCenter`, 'topic', {durable: false})
      await this.channel.bindQueue(this.myQueue, `exClubCenter`, `club:${this.gameName}:${clubId}`)
      this.clubId = clubId;
    }
  }

  async cancelListenClub(clubId = -1) {
    if (this.channel && clubId) {
      await this.channel.unbindQueue(this.myQueue, `exClubCenter`, `club:${this.gameName}:${clubId}`)
    }
  }

  updateGoldGemRuby(data) {
    if (this.model) {
      this.model.gold += data.gold || 0
      this.model.coin += data.gem || 0
      this.model.ruby += data.ruby || 0
    }
  }

  requestTo(queue, name, message) {
    const playerIp = this.getIpAddress()
    this.channel.sendToQueue(
      queue,
      this.toBuffer({name, from: this._id, payload: message, ip: playerIp}),
      {replyTo: this.myQueue})
  }

  requestToCurrentRoom(name, message = {}) {
    const playerIp = this.getIpAddress()
    if (!this.currentRoom) {
      logger.error('player is not in room', name, message)
      return
    }

    logger.verbose(name, message)
    try {
      this.channel.publish(
        'exGameCenter',
        `${this.gameName}.${this.currentRoom}`,
        this.toBuffer({name, from: this._id, payload: message, ip: playerIp}),
        {replyTo: this.myQueue})
    } catch (e) {

    }
  }

  //前端强制解散
  // forceCloseRoom(gameName, roomNum){
  //   this.channel.publish(
  //     'exGameCenter',
  //     `${gameName}.${roomNum}`,
  //     this.toBuffer({name:'forceDissolve'}),
  //     {replyTo: this.myQueue})
  // }

  setGameName(gameType) {
    this.gameName = gameType || 'doudizhu';
  }

  forceCloseRoom(gameName = 'doudizhu', roomNum) {
    this.channel.publish(
      'exGameCenter',
      `${gameName}.${roomNum}`,
      this.toBuffer({name: 'forceDissolve'}),
      {replyTo: this.myQueue})
  }


  requestToRoom(roomId, name, message) {
    const playerIp = this.getIpAddress()
    this.channel.publish(
      'exGameCenter',
      `${this.gameName}.${roomId}`,
      this.toBuffer({name, from: this._id, payload: message, ip: playerIp}),
      {replyTo: this.myQueue})
  }

  emit(event: string, message): boolean {
    super.emit(event, message)
    this.requestToCurrentRoom(event, message)
    return true
  }

  toBuffer(messageJson) {
    return new Buffer(JSON.stringify(messageJson))
  }

  toJSON() {
    return this.model._id
  }

  updateResource2Client() {
    this.sendMessage('resource/update', pick(this.model, ['gold', 'gem', 'ruby']))
  }
}


