import {ModelType, pre, prop, Typegoose} from 'typegoose'

export const RMB_2_COIN_RATE = 10

@pre<Product>(`save`, async function (next) {

  if (this.stock <= 0)
    this.state = 'off'

  next()
})

// @index({brand: 1, series: 1, model: 1}, {unique: false})
export class Product extends Typegoose {

  @prop({required: true, unique: true, sparse: true})
  skuId: string

  @prop({required: true})
  category: string

  @prop({required: true})
  brand: string

  @prop({required: true})
  series: string

  @prop({required: true})
  name: string

  @prop({required: true, default: false})
  authorized: boolean

  @prop({required: true})
  state: 'on' | 'off' | 'preview' | 'limited'

  @prop({required: true, default: false})
  onPointMarket: boolean

  @prop({required: true, default: 10000})
  pointPrice: number

  @prop({required: true, default: 1})
  pointRedeem: number // 商品退成积分

  @prop({required: false})
  trackId: string

  @prop({required: true})
  model: string

  @prop({required: true})
  gram: number

  @prop({required: true, default: 0})
  stock: number

  @prop({required: true})
  wrap: string

  @prop({required: true})
  expireAt: Date

  @prop({required: true})
  coverUrl: string

  @prop({required: true})
  description: string

  @prop({required: true})
  marketPrice: number

  @prop({required: true})
  underLinePrice: number // 划线价格

  @prop({required: true})
  promotePrice: number

  @prop({required: true})
  otherPrice: {}

  @prop({required: false})
  tag: string

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date

  @prop({required: true, default: 50})
  sortNum: number
}

export const ProductModel: ModelType<Product> = new Product().getModelForClass(Product)
