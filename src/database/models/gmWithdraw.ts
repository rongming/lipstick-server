import {index, prop, Ref, Typegoose} from 'typegoose'

class GM extends Typegoose {
}

@index({gm: 1})
@index({createAt: -1})
export class GmWithdraw extends Typegoose {
  @prop({required: true, ref: 'GM'})
  gm: Ref<GM>

  @prop({required: true})
  commission: number

  @prop({required: true})
  state: 'created' | 'finished' | 'refused'

  @prop({required: true})
  createAt: Date

  @prop({required: false})
  finishedAt: Date
}

export const GmWithdrawModel = new GmWithdraw().getModelForClass(GmWithdraw)
