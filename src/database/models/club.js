'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClubSchema = new Schema({
  owner: {type: String, required: true, ref: 'Player'},
  shortId: {type: Number, required: true},
  name: {type: String, required: true},
  gameType: {type: String, required: true},
  createAt: {type: Date, required: true, default: Date.now},
  defaultRule: {type: Object}
});

ClubSchema.index({createAt: -1});
ClubSchema.index({owner: 1});
ClubSchema.index({shortId: 1}, {unique: true});

const Club = mongoose.model('Club', ClubSchema);

export default Club
