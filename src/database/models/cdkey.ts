import {arrayProp, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Player} from "./player";

export class Cdkey extends Typegoose {
  @prop({required: true})
  name: string

  @prop({required: true})
  keys: string[]

  @prop({required: true})
  type: 'OneToAll' | 'OneToSome' | 'AllToAll'

  @arrayProp({required: true, itemsRef: Player, default: []})
  toPlayers: Ref<Player>[]

  @prop({required: true, default: 0})
  coin: number

  @prop({required: true, default: 0})
  freeCoin: number

  @prop({required: true, default: 0})
  gem: number

  @prop({required: true, default: 0})
  point: number

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date

  @prop({required: true})
  expireAt: Date

  @prop({required: true})
  state: 'on' | 'off'

  @prop()
  remarks: string // 备注
}

export const CdkeyModel: ModelType<Cdkey> = new Cdkey().getModelForClass(Cdkey)
