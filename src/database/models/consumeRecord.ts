import {Types} from "mongoose";
import {arrayProp, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Player} from "./player";

export class ConsumeRecord extends Typegoose {
  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  objectId: Types.ObjectId

  @prop({required: true})
  coin: number

  @prop({required: true})
  freeCoin: number

  @prop({required: true})
  gem: number

  @prop({required: true})
  point: number

  @prop({required: true, default: 0})
  fangKa: number

  @prop({required: true})
  payFor: 'lipStick' | 'rotateEgg' | 'resumeActivity' | 'randomLuckBox' | 'pressAccurate'

  @prop({required: true})
  success: boolean  // 扭蛋礼物盒

  @arrayProp({itemsRef: 'GM', default: []})
  relation: Types.ObjectId[]

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  endAt: Date
}

export const ConsumeRecordModel: ModelType<ConsumeRecord> = new ConsumeRecord().getModelForClass(ConsumeRecord)
