'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId

export const SELF_CATEGORY = 'paodekuai'

const RoomRecordSchema = new Schema({
  room: {type: String, required: true},
  players: {type: [String], required: true, ref: 'Player'},
  scores: {type: [], required: true},
  createAt: {type: Date, required: true, 'default': Date.now},
  roomNum: {type: String, required: true},
  creatorId: {type: Number },
  rule: {type: Object, required: true},
  category: {type: String, required: true, 'default': SELF_CATEGORY},
  club: {type: ObjectId, ref: 'Club'},
  checked: {type: Boolean, required: true, 'default': false},
});

RoomRecordSchema.index({players: 1});
RoomRecordSchema.index({room: 1});
RoomRecordSchema.index({createAt: -1});
RoomRecordSchema.index({club: 1, createAt: -1});

const RoomRecord = mongoose.model('RoomRecord', RoomRecordSchema);

export default RoomRecord;
