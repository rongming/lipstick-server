import * as mongoose from "mongoose"
import {index, ModelType, prop, Typegoose} from "typegoose";
import {AuthInfo} from "../../api/wechatAuth";

@index({gem: -1})
@index({createAt: -1})
@index({wechatOpenId: -1})
@index({weChatUid: 1}, {unique: true, sparse: true})
export class Player extends Typegoose {

  @prop()
  wechatOpenId: string

  @prop()
  wechatUnionid: string

  @prop({required: true})
  shortId: number

  @prop()
  wechatBind: AuthInfo

  @prop({required: true})
  name: string

  @prop({required: true})
  sex: number

  @prop({default: ''})
  headImgUrl: string

  @prop({required: true, default: true})
  isTourist: boolean

  @prop({required: true, default: 0})
  coin: number

  @prop({required: true, default: 0})
  freeCoin: number

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: ''})
  cookie: string

  @prop({required: true, default: 0})
  gold: number

  @prop({required: true, default: 0})
  gem: number

  @prop({required: true, default: 0})
  point: number

  @prop({required: true, default: 0})
  ruby: number

  @prop({sparse: true, unique: true})
  phone: string

  @prop({ref: 'GM'})
  inviteBy: mongoose.Types.ObjectId

  @prop({ref: Player})
  shareBy: mongoose.Types.ObjectId

  @prop({required: true, default: 0})
  sharedCount: number
}

export const PlayerModel: ModelType<Player> = new Player().getModelForClass(Player)

export const getLevel = (m: any) => {
  return 0
}
