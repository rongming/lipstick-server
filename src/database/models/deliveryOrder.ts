import {arrayProp, index, prop, Ref, Typegoose} from "typegoose";
import {Address} from "./address";
import {Delivery} from "./delivery";
import {GamePrize} from "./gamePrize";
import {Player} from "./player";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class DeliveryOrder extends Typegoose {

  @prop({required: true, ref: Delivery})
  delivery: Ref<Delivery>

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @arrayProp({itemsRef: GamePrize, required: true})
  prizes: Ref<GamePrize>[]

  @prop({required: true, ref: Address})
  toAddress: Ref<Address>

  @prop({required: true})
  RMBCost: number

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true, default: () => new Date(), index: true})
  createAt: Date
}

export const DeliveryOrderModel = new DeliveryOrder().getModelForClass(DeliveryOrder)
