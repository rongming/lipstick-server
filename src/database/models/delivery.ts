import {ModelType, prop, Ref, Typegoose, arrayProp, pre} from "typegoose";
import {Player} from "./player";
import {Address} from "./address";
import {GamePrize} from "./gamePrize";

@pre<Delivery>('save', function (next) {

  if (this.prizes && this.prizes.length === 0)
    throw Error('prizes 不能为空')

  next()
})

export class Delivery extends Typegoose {
  @prop({required: true, ref: Player})
  player: Ref<Player>

  @arrayProp({itemsRef: GamePrize, required: true})
  prizes: Array<Ref<GamePrize>>

  @prop({ref: Address})
  toAddress: Ref<Address>

  @prop({required: true})
  fee: number

  @prop({required: true})
  state: 'havePaid' | 'prepared' | 'delivering' | 'delivered'

  @prop()
  carrier: string

  @prop()
  trackingNumber: string

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date(0)})
  outAt: Date

  @prop({default: () => new Date()})
  endAt: Date
}

export const DeliveryModel: ModelType<Delivery> = new Delivery().getModelForClass(Delivery)
