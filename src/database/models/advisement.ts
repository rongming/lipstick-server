import {index, prop, Ref, Typegoose} from 'typegoose'
import {Player} from "./player";


export enum AdvisementState {
  UNREAD = 'unread',
  READ = 'read',
  HANDLED = 'handled',
}


@index({player: -1})
@index({createAt: -1})
@index({phoneNum: -1})
@index({qqNum: -1})
export class Advisement extends Typegoose {
  @prop({required: true, default:'建议/反馈'})
  title: string

  @prop({required: true})
  content: string

  @prop({required: true, ref: Player})
  from: Ref<Player>

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date

  @prop()
  phoneNum: string

  @prop()
  qqNum: string

  @prop({default: AdvisementState.UNREAD, required: true})
  state: AdvisementState.UNREAD | AdvisementState.READ | AdvisementState.HANDLED

  @prop()
  reply: string
}
const AdvisementModel = new Advisement().getModelForClass(Advisement);

export default AdvisementModel

