import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Player} from "./player";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class RechargeWithOtherGameOrder extends Typegoose {
  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  type: 'exchange' | 'consume' | 'refund'

  @prop({required: true, default: 'notGame'})
  gameType: string

  @prop({required: true, default: 0})
  otherMoney: number

  @prop({required: true})
  coin: number

  @prop({required: true})
  freeCoin: number

  @prop({required: true})
  gem: number

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: false, default: () => new Date()})
  finishAt: Date
}

export const RechargeWithOtherGameOrderModel: ModelType<RechargeWithOtherGameOrder> = new RechargeWithOtherGameOrder().getModelForClass(RechargeWithOtherGameOrder)
