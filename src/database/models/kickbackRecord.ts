import {Types} from "mongoose";
import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Player} from "./player";

@index({from: 1, createAt: -1})
export class KickbackRecord extends Typegoose {

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  RMBCost: number

  @prop({required: true})
  type: 'recharge' | 'buy'

  @prop({required: true})
  fromId: Types.ObjectId

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({ref: 'GM'})
  level2: Types.ObjectId
  @prop({required: true, default: 0})
  kickback2: number

  @prop({ref: 'GM'})
  level1: Types.ObjectId
  @prop({required: true, default: 0})
  kickback1: number

  @prop({ref: 'GM'})
  super: Types.ObjectId
  @prop({required: true, default: 0})
  kickbackAll: number

}

export const KickbackRecordModel: ModelType<KickbackRecord> = new KickbackRecord().getModelForClass(KickbackRecord)
