import {ModelType, prop, Ref, Typegoose} from "typegoose";
import {Cdkey} from "./cdkey";
import {Player} from "./player";

export class CdkeyRecord extends Typegoose {
  @prop({required: true, ref: Cdkey})
  cdkey: Ref<Cdkey>

  @prop({ref: Player})
  player: Ref<Player>

  @prop({required: true})
  keyWord: string

  @prop({required: true, default: 0})
  coin: number

  @prop({required: true, default: 0})
  freeCoin: number

  @prop({required: true, default: 0})
  gem: number

  @prop({required: true, default: 0})
  point: number

  @prop({required: true, default: () => new Date()})
  createAt: Date
}

export const CdkeyRecordModel: ModelType<CdkeyRecord> = new CdkeyRecord().getModelForClass(CdkeyRecord)
