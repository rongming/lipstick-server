import {ModelType, prop, Typegoose} from "typegoose";

export class RechargeWithOtherGame extends Typegoose {

  @prop({required: true})
  gameType: string

  @prop({required: true, default: 0})
  otherMoney: number

  @prop({default: 0})
  coin: number

  @prop({default: 0})
  freeCoin: number

  @prop({default: 0})
  gem: number

  @prop({required: true})
  state: 'on' | 'off'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date
}

export const RechargeWithOtherGameModel: ModelType<RechargeWithOtherGame> = new RechargeWithOtherGame().getModelForClass(RechargeWithOtherGame)
