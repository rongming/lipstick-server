import * as mongoose from "mongoose";
import {index, prop, Ref, Typegoose} from 'typegoose'
import GM from "./gm";
import {Player} from "./player";
import {Product} from "./Product";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class ProductOrder extends Typegoose {

  @prop({required: true, ref: Product})
  product: Ref<Product>

  @prop({required: true})
  productName: string

  @prop({required: true})
  productDescription: string

  @prop({required: true})
  productCoverUrl: string

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: false, default: () => new Date()})
  finishAt: Date

  @prop({ref: GM})
  inviteBy: mongoose.Schema.Types.ObjectId

  @prop({required: true})
  RMBCost: number
}

export const ProductOrderModel = new ProductOrder().getModelForClass(ProductOrder)
