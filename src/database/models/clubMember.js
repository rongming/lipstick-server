'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId

const ClubMemberSchema = new Schema({
  club: {type: ObjectId, required: true, ref: 'Club'},
  member: {type: String, required: true, ref: 'Player'},
  gameType: {type: String, required: true},
  joinAt: {type: Date, required: true, default: Date.now},
});

ClubMemberSchema.index({joinAt: -1});
ClubMemberSchema.index({member: 1});
ClubMemberSchema.index({club: 1, member: 1});

const ClubMember = mongoose.model('ClubMember', ClubMemberSchema);

export default ClubMember
