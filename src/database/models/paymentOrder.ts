import {Types} from "mongoose";
import {ModelType, prop, Typegoose, Ref, index, InstanceType, arrayProp, pre} from 'typegoose'


@index({order: 1})
export class PaymentOrder extends Typegoose {


  @prop({required: true})
  paymentType: 'wechatPay' | 'aliPay'

  @prop({required: true})
  orderType: string

  @prop({required: true})
  order: Types.ObjectId

  @prop({required: true})
  externalId: string

  @prop({required: true})
  state: 'init' | 'finished' | 'error'

  @prop({required: true})
  appId: string

  @prop({required: true})
  price: number

  @prop({required: true})
  rawOrder: any

  @prop({required: true, default: () => new Date()})
  createAt: Date
}

export const PaymentOrderModel: ModelType<PaymentOrder> = new PaymentOrder().getModelForClass(PaymentOrder)
