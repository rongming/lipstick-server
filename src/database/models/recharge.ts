import {ModelType, prop, Typegoose} from "typegoose";

export class Recharge extends Typegoose {

  @prop({required: true})
  name: string

  @prop({})
  remarks: string

  @prop({required: true})
  RMBCost: number

  @prop({default: 0})
  coin: number

  @prop({default: 0})
  freeCoin: number

  @prop({default: 0})
  gem: number

  @prop({required: true})
  state: 'on' | 'off'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date
}

export const RechargeModel: ModelType<Recharge> = new Recharge().getModelForClass(Recharge)
