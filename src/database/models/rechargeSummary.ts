import {index, ModelType, prop, Typegoose} from "typegoose";

@index({day: 1})
export class RechargeSummary extends Typegoose {

  @prop({required: true})
  day: Date

  @prop({required: true})
  type: string

  @prop({required: true})
  sum: number

  @prop({required: true})
  recharges: number

  @prop({required: true, default: () => new Date()})
  createAt: Date
}

export const RechargeSummaryModel: ModelType<RechargeSummary> = new RechargeSummary().getModelForClass(RechargeSummary)
