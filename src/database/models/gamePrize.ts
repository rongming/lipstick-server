import * as mongoose from "mongoose";
import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Address} from "./address";
import {LipstickSeries} from "./lipstickSeries";
import {Player} from "./player";
import {Product} from "./Product";
import {ProductGroup} from "./ProductGroup";

@index({player: 1, createAt: -1, state: 1})
export class GamePrize extends Typegoose {
  @prop({ref: Player, required: true})
  player: Ref<Player>

  @prop({required: false, ref: LipstickSeries})
  lipstickSeries: Ref<LipstickSeries>

  @prop({required: false, ref: ProductGroup})
  pressGroup: Ref<ProductGroup>

  @prop({required: true, ref: Product})
  product: Ref<Product>

  @prop({required: true})
  productName: string

  @prop({required: true})
  productModel: string

  @prop({required: true})
  productDescription: string

  @prop({required: true})
  productUrl: string

  @prop()
  invitedBy: mongoose.Types.ObjectId

  @prop({required: true})
  state: 'idle' | 'prepared' | 'delivering' | 'delivered' | 'returned'

  @prop({required: true})
  selectState: 'waitSelect' | 'selected' | 'notAllowed'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop()
  deliveryId: string

  @prop()
  requestDeliveryAt: Date

  @prop({ref: Address})
  toAddress: Ref<Address>

  @prop({required: true})
  from: 'lipstick' | 'freeGift' | 'box' | 'egg' | 'luckyBox' | 'buy' | 'pressAccurate' | 'pointBuy'

  @prop()
  remarks: string // 实际获得的奖品 在 from === 'pressAccurate' 时会有值
}

export const GamePrizeModel: ModelType<GamePrize> = new GamePrize().getModelForClass(GamePrize)
