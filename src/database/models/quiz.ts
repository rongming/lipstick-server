import * as mongoose from 'mongoose'
import {prop, Typegoose, arrayProp} from "typegoose"


export class Quiz extends Typegoose {

  @prop({required: true, unique: true})
  question: string

  @prop({required: true})
  answer: string

  @arrayProp({required: true, items: String})
  wrongAnswers: string[]

  @prop({default: 0})
  used: number
  @prop({default: 0})
  rightCount: number
}


export const QuizModel = new Quiz().getModelForClass(Quiz)
