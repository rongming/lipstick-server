import {index, prop, Ref, Typegoose} from "typegoose";
import {GameActivity} from "./gameActivity";
import {LipstickSeries} from "./lipstickSeries";
import {Player} from "./player";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class LipstickGameOrder extends Typegoose {

  @prop({ref: GameActivity})
  activity: Ref<GameActivity>

  @prop({ref: LipstickSeries})
  lipstickSeries: Ref<LipstickSeries>

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true})
  payFor: 'start' | 'resume'

  @prop({required: true, default: () => new Date(), index: true})
  createAt: Date
}

export const LipstickGameOrderModel = new LipstickGameOrder().getModelForClass(LipstickGameOrder)
