import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import {Player} from "./player";

@index({player: 1, createAt: -1, state: 1})
export class Address extends Typegoose {
  @prop({ref: Player, required: true})
  player: Ref<Player>

  @prop({required: true})
  pcasCode: string

  @prop({required: true})
  province: string

  @prop({required: true})
  city: string

  @prop({required: true})
  area: string

  @prop({required: true})
  street: string

  @prop({required: true})
  detail: string


  @prop({required: true})
  phone: string

  @prop({required: true})
  receiver: string

  @prop({default: ''})
  alias: string

  @prop({required: true})
  'default': boolean

  @prop({required: true, default: () => new Date()})
  createAt: Date
}


export const AddressModel: ModelType<Address> = new Address().getModelForClass(Address)
