import {ModelType, prop, Ref, Typegoose} from "typegoose";
import {LipstickSeries} from "./lipstickSeries";
import {Player} from "./player";
import {Product} from "./Product";
import {PreProducts, PressGroup} from "./ProductGroup";

export class GameActivity extends Typegoose {
  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({ref: LipstickSeries})
  lipstickSeries: Ref<LipstickSeries>

  @prop({ref: PressGroup})
  pressGroup: Ref<PressGroup>

  @prop({required: false, ref: Product})
  preProduct: Ref<Product>

  @prop({required: false})
  preProducts: PreProducts

  @prop({default: false})
  canWin: boolean

  @prop({default: 0})
  winLevel: number

  @prop({default: false})
  realWin: boolean

  @prop({default: 0})
  realWinLevel: number

  @prop({required: true})
  feeInFen: number

  @prop({required: false})
  feeInFangka: number

  @prop({required: true, default: 0})
  resumeTimes: number

  @prop()
  stage: number

  @prop({required: false, default: 0})
  income: number

  @prop({required: true})
  from: 'lipstick' | 'pressAccurate'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  endAt: Date

  @prop({required: true, default: 'free'})
  state: 'free' | 'done'
}

export const GameActivityModel: ModelType<GameActivity> = new GameActivity().getModelForClass(GameActivity)
