import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import {LipstickSeries} from "./lipstickSeries";

export const DEFAULT_THRESHOLD_RATE = 2
export const DEFAULT_DAPAN_THRESHOLD_RATE = 2

@index({lipstickSeries: 1})
export class SeriesIncomeThreshold extends Typegoose {
  @prop({ref: LipstickSeries, required: true, unique: true})
  lipstickSeries: Ref<LipstickSeries>

  @prop({required: true, default: DEFAULT_THRESHOLD_RATE})
  thresholdCoinRate: number

  @prop({required: true, default: DEFAULT_DAPAN_THRESHOLD_RATE})
  dapanThresholdCoinRate: number

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: true, default: () => new Date()})
  updateAt: Date
}

export const SeriesIncomeThresholdModel: ModelType<SeriesIncomeThreshold>
  = new SeriesIncomeThreshold().getModelForClass(SeriesIncomeThreshold)
