import {index, prop, Ref, Typegoose} from "typegoose";
import {GameActivity} from "./gameActivity";
import {Player} from "./player";
import {PressGroup, ProductGroup} from "./ProductGroup";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class PressGameOrder extends Typegoose {

  @prop({ref: GameActivity})
  activity: Ref<GameActivity>

  @prop({required: true, ref: PressGroup})
  pressGroup: Ref<PressGroup>

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true, default: () => new Date(), index: true})
  createAt: Date
}

export const PressGameOrderModel = new PressGameOrder().getModelForClass(PressGameOrder)
