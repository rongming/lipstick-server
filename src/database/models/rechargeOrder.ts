import * as mongoose from "mongoose";
import {index, ModelType, prop, Ref, Typegoose} from "typegoose";
import GM from "./gm";
import {Player} from "./player";
import {Recharge} from "./recharge";

@index({player: 1, status: 1, createAt: -1})
@index({createAt: -1, status: 1})
export class RechargeOrder extends Typegoose {

  @prop({required: true, ref: Recharge})
  recharge: Ref<Recharge>

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true})
  RMBCost: number

  @prop({required: true})
  coin: number

  @prop({required: true})
  freeCoin: number

  @prop({required: true})
  gem: number

  @prop({required: true})
  state: 'unpaid' | 'paid'

  @prop({required: true, default: () => new Date()})
  createAt: Date

  @prop({required: false, default: () => new Date()})
  finishAt: Date

  @prop({ref: GM})
  inviteBy: mongoose.Schema.Types.ObjectId
}

export const RechargeOrderModel: ModelType<RechargeOrder> = new RechargeOrder().getModelForClass(RechargeOrder)
