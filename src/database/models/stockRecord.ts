import {index, prop, Ref, Typegoose} from "typegoose"
import {Product} from "./Product";


@index({product: 1})
export class StockRecord extends Typegoose {


  @prop({ref: Product, required: true})
  product: Ref<Product>

  @prop({required: true})
  amount: number

  @prop({required: true})
  note: string
}


export const StockRecordModel = new StockRecord().getModelForClass(StockRecord)
