import {index, prop, Ref, Typegoose} from 'typegoose'
import {Player} from "./player";

export enum MailType {
  MESSAGE = 'message',
  NOTICE = 'notice',
  GIFT = 'gift',
  NOTICEGIFT = 'noticeGift',
}

export enum MailState {
  UNREAD = 'unread',
  READ = 'read',
  DELETE = 'delete',
  REQUESTED = 'requested'
}

export enum GiftState {
  NOTALLOW = 'notAllow',
  AVAILABLE = 'available',
  REQUESTED = 'requested'
}

@index({player: -1})
@index({createAt: -1})
export class Mail extends Typegoose {
  @prop({required: true})
  title: string;

  @prop({required: true})
  content: string;

  @prop({required: true, ref: Player})
  to: Ref<Player>

  @prop({required: true, default: () => new Date()})
  createAt: Date;

  @prop({default: MailType.NOTICE, required: true})
  type: MailType.MESSAGE | MailType.NOTICE | MailType.GIFT | MailType.NOTICEGIFT

  @prop({default: MailState.UNREAD, required: true})
  state: MailState.UNREAD | MailState.READ | MailState.DELETE | MailState.REQUESTED

  @prop() /* coin:number ruby:number gold:number */
  gift: any

  @prop({required: true, default: GiftState.AVAILABLE})
  giftState: GiftState.AVAILABLE | GiftState.REQUESTED
}

export class PublicMail extends Typegoose {
  @prop({required: true})
  title: string;

  @prop({required: true})
  content: string;

  @prop({required: true, default: () => new Date()})
  createAt: Date;

  @prop({default: MailType.MESSAGE, required: true})
  type: MailType.MESSAGE | MailType.NOTICE | MailType.GIFT | MailType.NOTICEGIFT

  @prop() /* coin:number ruby:number gold:number */
  gift: any

}

@index({mail: -1})
export class PublicMailRecord extends Typegoose {

  @prop({required: true, ref: Player})
  player: Ref<Player>

  @prop({required: true, ref: PublicMail})
  mail: Ref<PublicMail>

  @prop({default: MailState.READ})
  state: MailState.READ | MailState.UNREAD | MailState.DELETE

  @prop({default: GiftState.AVAILABLE})
  giftState: GiftState.AVAILABLE | GiftState.REQUESTED
}

export const MailModel = new Mail().getModelForClass(Mail);

export const PublicMailModel = new PublicMail().getModelForClass(PublicMail)

export const PublicMailRecordModel = new PublicMailRecord().getModelForClass(PublicMailRecord)
