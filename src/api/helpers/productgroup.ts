import {InstanceType} from "typegoose";
import {ProductGroup, ProductGroupModel} from "../../database/models/ProductGroup";

export async function findProductGroupById(id: string)
  : Promise<InstanceType<ProductGroup>> {
  const pg = await ProductGroupModel.findById(id)
    .populate('level1')
    .populate('level2')
    .populate('level3')
    .populate('level4')
  return pg
}

