import {sign} from "jsonwebtoken";

export const jwtSecret = 'KkBoRrNOr1b5/PD0QmOmXOtAQ9E=';

export function jwtSign(payload, expire: string = '30day') {
  return sign(payload, jwtSecret, {algorithm: 'HS256', expiresIn: expire})
}
