import {Delivery} from "../database/models/delivery";
import {ProductOrderModel} from "../database/models/ProductOrder";

export class PaymentDispatcher {


  async handle(payment: { type: string, order: string }) {

    const order = await ProductOrderModel.findById(payment.order)

    if (order.state === 'unpaid') {

      order.state = 'paid'
      await order.save()

    }

  }
}
