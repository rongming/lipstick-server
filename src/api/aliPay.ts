import {Router} from 'express'
import {alipayCashier} from "../alipay/payment";
import {paymentDispatcher} from "./dispatcher";
import bodyParser = require("body-parser");


const router = Router()

function aliParse() {
  return bodyParser.urlencoded({
    extended: true,
    type: function typeMatcher(req) {
      return req.headers['content-type'] === 'application/x-www-form-urlencoded; text/html; charset=utf-8'
    }
  })
}


router.post('/notify', aliParse(), async (req, res) => {

  try {

    await alipayCashier.confirmPayment(req.body, paymentDispatcher)
    res.status(200).send("success")
  } catch (e) {
    console.log(e)
    res.status(500).send(".oOps")
  }
})

export default router

