import {Router} from "express";

import * as xmlParser from 'express-xml-bodyparser'
import {wechatPayCashier} from "../wechat/wechatCashier";
import {paymentDispatcher} from "./dispatcher";
import {Response} from "./middlewareType"

const router = Router()


const wechatNotificationParser = xmlParser({explicitArray: false})

router.post('/notify', wechatNotificationParser, async (req, res: Response) => {

  try {
    await wechatPayCashier.confirmPayment(req.body.xml, paymentDispatcher)
    res.send(`<xml><return_code><![CDATA[SUCCESS]]></return_code></xml>`)
  } catch (e) {
    res.error(e)
  }

})


export default router
