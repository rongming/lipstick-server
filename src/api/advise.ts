import * as express from 'express'
import {jwtAuthMiddleware} from "./passort";
import AdvisementModel from '../database/models/advisement'
import {celebrate, Joi} from "celebrate";
import {Response, withUser} from "./middlewareType";
import {InternalError} from "./error";
import {PlayerModel} from "../database/models/player";


const router = express.Router()
export default router

router.post('/',
  jwtAuthMiddleware,
  celebrate({
    body: Joi.object().keys({
      content: Joi.string().required(),
      title: Joi.string().default('建议/反馈'),
      phoneNum: Joi.string(),
      qqNum: Joi.string()
    })
  }),
  async (req: express.Request & withUser, res: Response) => {
    const player = await PlayerModel.findById(req.user._id)
    if(!player) {
      return res.error(InternalError('NO_SUCH_USER'))
    }
    await AdvisementModel.create({
      title: req.body.title,
      content: req.body.content,
      phoneNum: req.body.phoneNum,
      qqNum: req.body.qqNum,
      from: req.user._id,
    })

    res.json({ok: true, data: {info: '反馈提交成功！'}})
  })

