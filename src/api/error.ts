export const ErrorCode2Message = {
  BAD_PCAS: '地区码错误',
  TOO_MANY_ADDRESS: '每个玩家只能创建5个地址',
  NO_SUCH_ADDRESS: '无此地址',
  NO_ON_STOCK: '此商品已下架',
  NO_ENOUGH_STOCK: '此商品已下架',
  NO_SUCH_PRODUCT: '库存不足',
  NO_SUCH_SERIES: '无此系列',
  NO_POINT_MARKET_PRODUCT: '积分商城暂未上架商品',
  NO_SUCH_RECHARGE: '无此充值',
  NO_SUCH_PRODUCT_GROUP: '无此商品组',
  NO_ENOUGH_COIN: '金币不足',
  NO_ENOUGH_GEM: '钻石不足',
  NO_ENOUGH_POINT: '积分不足',
  ALREADY_CLAIMED: '游戏已结束',
  FAKE_CLIENT: '请使用正版客户端',
  AUTH_EXPIRED: '鉴权已过期',
  NO_SUCH_USER: '无此用户',
  NO_SUCH_INVITE_CODE: '无此邀请码',
  NO_SUCH_PRIZE: '无此奖品',
  NO_SUCH_GIFT_MAIL: '无此奖励邮件',
  ALREADY_SELECTED_PRIZE: '已选过类别或者无法选择',
  ALREADY_IN_DELIVERY: '发货中',
  MAIL_FETCH_ERROR: '邮件获取错误',
  BAD_WEHCAT_PAY_NOTIFICATION: '错误的通知[wechat]',
  BAD_ALI_PAY_NOTIFICATION: '错误的通知[aliPay]',
  ALREADY_CONFIRMED_PAYMENT: '已经确认付款',
  NO_SUCH_WECHAT_PAYMENT: '无此支付订单[wechat]',
  NO_SUCH_ALI_PAYMENT: '无此支付订单[alipay]',
  ALREADY_BOUND_WECHAT_ACCOUNT: '已经绑定微信账号',
  SAME_PRODUCT_DETAIL_ID: '相同产品详情ID',
  NO_ENOUGH_INCOME: '幸运值不足',
  NO_PAYTYPE_SELECTED: '未选择支付方式',
  ALREADY_ADD_DELIVERY: '已生成运单',
  OVER_RESUME_LIMIT: '超出复活次数限制',
  ERROR_PHONE_NUMBER: '手机号码错误',
  ERROR_INVITE_CODE: '邀请码错误',
  NO_PHONE_NUMBER_REGISTERED: '手机号未注册',
  GAME_TIME_UP: '游戏状态已超时',
  EMPTY_NICKNAME_REFUSED: '昵称不能为空',
  TOO_LONG_NICKNAME: '昵称太长',
  ERROR_SEX: '性别有误',
  ERROR_CD_KEY: '兑换码有误',
  ERROR_PLAYER_LINK_TO_CD_KEY: '这不是您的专属兑换码',
  EXPIRED_CD_KEY: '兑换码已过期',
  ALREADY_USE_CD_KEY: '您已使用过该兑换码'
}

export type ErrorCode = keyof typeof ErrorCode2Message

export type InternalError = Error & { message: ErrorCode }

export function InternalError(code: ErrorCode) {
  return Error(code)
}
