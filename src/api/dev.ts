import {celebrate, Joi} from "celebrate";
import {Router, Request} from "express";
import {PlayerModel} from "../database/models/player";
import {withUser} from "./middlewareType";
import {jwtAuthMiddleware} from "./passort";


const router = Router()


router.post('/resource',
  jwtAuthMiddleware,
  celebrate({
    body: Joi.object().keys({
      coin: Joi.number().integer().greater(0).required(),
      gem: Joi.number().integer().greater(0).required(),
    })
  }),
  async (req: withUser & Request, res) => {

    const updated = await PlayerModel.findByIdAndUpdate(req.user.id, {$inc: {...req.body}}, {new: true})

    res.json({
      ok: true,
      data: {player: updated}
    })
  }
)

export default router
