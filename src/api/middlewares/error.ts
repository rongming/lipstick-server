import {ErrorCode2Message, ErrorCode} from "../error";
import {Response} from "../middlewareType";

export default function error(req, res: Response, next) {

  res.error = function (error: Error) {
    if (!ErrorCode2Message[error.message]) {
      console.error(error, error.stack)
    }

    const message = error.message
    res.json({
      ok: false,
      error: message,
      msg: ErrorCode2Message[message] || '服务器错误'
    })
  }

  next()
}


