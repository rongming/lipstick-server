import * as express from 'express'
import {InstanceType} from "typegoose";
import {Address} from "../database/models/address";
import {Cdkey} from "../database/models/cdkey";
import {Delivery} from "../database/models/delivery";
import {GameActivity} from "../database/models/gameActivity";
import {GamePrize} from "../database/models/gamePrize";
import {LipstickSeries} from "../database/models/lipstickSeries";
import {Player} from "../database/models/player";
import {Product} from "../database/models/Product";
import {PressGroup, ProductGroup} from "../database/models/ProductGroup";
import {Recharge} from "../database/models/recharge";

export type withProduct = { product: InstanceType<Product> }
export type withUser = { user: InstanceType<Player> }
export type withType = { smsCodeType: string }
export type withAddress = { address: InstanceType<Address> }
export type withActivity = { activity: InstanceType<GameActivity> }
export type withProductGroup = { productGroup: InstanceType<ProductGroup> }
export type withPressGroup = { pressGroup: InstanceType<PressGroup> }
export type withRecharge = { recharge: InstanceType<Recharge> }
export type withLipstickSeries = { lipstickSeries: InstanceType<LipstickSeries> }
export type withGamePrize = { gamePrize: InstanceType<GamePrize> }
export type withDelivery = { delivery: InstanceType<Delivery> }
export type withCdKey = { cdKey: InstanceType<Cdkey> }



type error = { error(error: Error): void }

export type Response = express.Response & error
