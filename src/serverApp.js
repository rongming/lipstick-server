/**
 *
 * Created by user on 2016-07-02.
 */
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as logger from 'winston';
import api from './api'
import addressRouter from "./api/address";
import gameRouter from './api/game'
import {jwtSecret} from "./api/jwt";
import {initJWTPassport} from "./api/passort";
import playerRouter from './api/player'
import prizeRouter from "./api/prize";
import productRouter from './api/product'
import lipstickSeriesRouter from './api/lipstickSeries'
import productOrderRouter from './api/productOrder'
import deliveryRouter from './api/delivery'
import router from './api/mail'
import adviseRouter from './api/advise'
import rechargeRouter from './api/recharge'
import rechargeOrderRouter from './api/rechargeOrder'
import wechatPayRouter from './api/wechatPay'
import aliPayRouter from './api/aliPay'
import loginRouter from "./api/login"
import devRouter from './api/dev'
import config from './utils/config';
import error from "./api/middlewares/error"

logger.level = config.getDefault('logger.level', 'info');
const passport = initJWTPassport(jwtSecret)

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.use(cors())

app.get('/', (req, res) => (
  res.send('<h1>Hello world!</h1>')
));


app.use(passport.initialize())

app.use(error)

app.use('/player', playerRouter)
app.use('/product', productRouter)
app.use('/lipstickSeries', lipstickSeriesRouter)
app.use('/productorder', productOrderRouter)
app.use('/recharge', rechargeRouter)
app.use('/rechargeorder', rechargeOrderRouter)
app.use('/delivery', deliveryRouter)
app.use('/mail', router)
app.use('/advise', adviseRouter)
app.use('/game', gameRouter)
app.use('/player/address', addressRouter)
app.use('/player/prize', prizeRouter)
app.use('/wechatpay', wechatPayRouter)
app.use('/alipay', aliPayRouter)
app.use('/login', loginRouter)

if (process.env.NODE_ENV !== 'production') {
  app.use('/dev', devRouter)
}


app.use('/public', express.static(`${__dirname}/../public`));

app.use('/authfail', (req, res) => {
  res.json({
    ok: false,
    error: 'TOKEN_EXPIRED',
    msg: '登录失效'
  })
})

app.use(api)

app.use((err, req, res, next) => {
  res.error(err)
})

export default app;
