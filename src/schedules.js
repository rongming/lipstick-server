import * as schedule from "node-schedule"
import * as config from "config"
import * as mongoose from 'mongoose'

// import cleanRecord from './bin/cleanRecord'
import aggregateRechargeRecord from "./bin/aggregateExtRecord";
import broadcastGame from "./bin/broadcastGame";

mongoose.connect(config.database.url)

schedule.scheduleJob('0 4,14 * * *', () => {
  // cleanRecord()
})


schedule.scheduleJob('0 */1 * * *', function () {
  console.log(`aggregateExtRecord`, new Date());
  aggregateRechargeRecord(new Date())
    .catch(error => {
      console.log('aggregateRechargeRecord error', error.stack)
    })
})

schedule.scheduleJob('*/30 * * * * *', function () {
  console.log(`broadcastGame`, new Date());
  broadcastGame()
    .catch(error => {
      console.log('broadcastGame error', error.stack)
    })
})
