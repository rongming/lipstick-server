export enum GameNames {
  mahjong = 'majiang',
  biaofen = 'biaofen',
  niuniu = 'niuniu',
  paodekuai = 'paodekuai',
  shisanshui = 'shisanshui',
  zhadan = 'zhadan',
  doudizhu = 'doudizhu'
}
