import {connect} from "amqplib";
import * as config from 'config'
import {TornamentManager, tournamentConfigs} from "./player/message-handlers-rmq/tornament";
import {logger} from "./utils/logger";
import createClient from "./utils/redis";
import ms = require("ms");


async function main() {
  const connection = await connect(config.get("amqp.url"))

  const ch = await connection.createChannel()
  const redisClient = createClient()

  const manager = new TornamentManager(redisClient, ch)


  setInterval(async () => {

    for (const tc of tournamentConfigs) {
      try {

        await manager.putRobotToTornamentQueue(tc.gameType, tc)

      } catch (e) {
        logger.error({tc, error: e.message, stack: e.stack})
      }
    }
  }, ms('7s'))

}


main()
  .catch((e) => {
    logger.error({
      scope: 'start robot main',
      message: e.message,
      stack: e.stack
    })
  })

